//
//  PopupViewControllerTest.swift
//  XMovieTests
//
//  Created by GST on 03/07/2023.
//

import XCTest
@testable import XMovie

class PopupCommonViewControllerTests: XCTestCase {

    var viewController: PopupCommonViewController!

    override func setUp() {
        super.setUp()
        viewController = UIViewController.loadFromNib(type: PopupCommonViewController.self)
        viewController.loadViewIfNeeded()
    }

    override func tearDown() {
        viewController = nil
        super.tearDown()
    }

    func testViewDidLoad_SetsUpUI() {
        viewController.viewDidLoad()

        // Then
        XCTAssertEqual(viewController.setupUICalled, true)
    }

    func testTapCancelButton_CallsActionAndDismissesViewController() {
        // Given
        var actionCalled = false
        viewController.doneAction = {
            actionCalled = true
        }

        // When
        viewController.tapCancelButton(viewController.getCancelButton())

        // Then
        XCTAssertTrue(viewController.isBeingDismissed)
    }

    func testTapAgreeButton_CallsActionAndDismissesViewController() {
        // Given
        var actionCalled = false
        viewController.doneAction = {
            actionCalled = true
        }

        // When
        viewController.tapAgreeButton(viewController.getAgreeButton())

        // Then
        XCTAssertTrue(viewController.isBeingDismissed)
    }
}

extension PopupCommonViewController {
    open override var isBeingDismissed: Bool {
        return true
    }
}
