//
//  ProfileViewControllerTest.swift
//  XMovieTests
//
//  Created by GST on 03/07/2023.
//

import XCTest
@testable import XMovie

class ProfileScreenViewControllerTests: XCTestCase {
    var viewController: ProfileScreenViewController!
    var presenter: ProfileScreenPresenter!
    var window: UIWindow!

    override func setUp() {
        super.setUp()
        window = UIWindow()
        viewController = ProfileScreenViewController.initFromStoryboard(feature: .profile)
        viewController.loadViewIfNeeded()
        presenter = ProfileScreenPresenter(model: ProfileScreenModel())
        viewController.presenter = presenter
        window.addSubview(viewController.view)
    }

    func testSetupTableView() {
        // When
        viewController.setupTableView()

        // Then
        XCTAssertTrue(viewController.registerCellCalled)
        XCTAssertEqual(viewController.mainTableView.delegate as? ProfileScreenViewController, viewController)
        XCTAssertEqual(viewController.mainTableView.dataSource as? ProfileScreenViewController, viewController)
        XCTAssertEqual(viewController.mainTableView.separatorStyle, .none)
        XCTAssertEqual(viewController.mainTableView.backgroundColor, .black)
    }

    func testNumberOfRowsInSection() {
        // Given
        let tableView = UITableView()

        // When
        let numberOfRows = viewController.tableView(tableView, numberOfRowsInSection: 0)

        // Then
        XCTAssertEqual(numberOfRows, ProfileScreenSection.allCases.count)
    }
    
    func testCellForItemAtAvatar() {
        // Given
        let indexPath = IndexPath(row: 0, section: 0)

        // When
        let cell = viewController.tableView(viewController.mainTableView, cellForRowAt: indexPath)

        // Then
        XCTAssertTrue(cell is AvatarTableCell)
    }
    
    func testCellForItemAtAccount() {
        // Given
        let indexPath = IndexPath(row: 1, section: 0)

        // When
        let cell = viewController.tableView(viewController.mainTableView, cellForRowAt: indexPath)

        // Then
        XCTAssertTrue(cell is AccountTableCell)
    }
    
    func testCellForItemAtGenaral() {
        // Given
        let indexPath = IndexPath(row: 2, section: 0)

        // When
        let cell = viewController.tableView(viewController.mainTableView, cellForRowAt: indexPath)

        // Then
        XCTAssertTrue(cell is GenaralTableCell)
    }

    func testHeightForRowAt() {
        // Given
        let tableView = UITableView()
        let indexPath = IndexPath(row: 0, section: 0)

        // When
        let rowHeight = viewController.tableView(tableView, heightForRowAt: indexPath)

        // Then
        XCTAssertEqual(rowHeight, UITableView.automaticDimension)
    }

}

extension ProfileScreenViewController {
    var registerCellCalled: Bool {
        return true
    }
    
    func isTableViewRegistered<T: UITableViewCell>(_ cellType: T.Type) -> Bool {
           let reuseIdentifier = String(describing: cellType)
           if let registeredCellIdentifiers = mainTableView.value(forKey: "_cellClassDict") as? [String: Any] {
               return registeredCellIdentifiers.keys.contains(reuseIdentifier)
           }
           return false
       }
}
