//
//  DetailPresenterTest.swift
//  XMovieTests
//
//  Created by GST on 30/06/2023.
//

import XCTest
@testable import XMovie

class DetailMoviePresenterTests: XCTestCase {
    var presenter: DetailMoviePresenter!
    var mockModel: MockDetailMovieModel!
    var mockView: MockDetailMovieView!
    
    override func setUp() {
        super.setUp()
        mockModel = MockDetailMovieModel()
        presenter = DetailMoviePresenter(model: mockModel)
        mockView = MockDetailMovieView()
        presenter.attach(view: mockView)
    }
    
    override func tearDown() {
        presenter.detachView()
        super.tearDown()
    }
    
    func testRequestData() async {
        // Prepare
        let movieID = 123
        let mockDetailMovie = MockData.detailMovie
        let mockListCast = MockData.dataCast
        let mockSimilarMovies = MockData.homeCommingMovieEntity
        mockModel.getDetailMoviesResult = Result { mockDetailMovie }
        mockModel.getListCategoryResult = Result { mockListCast }
        mockModel.getSimilarMoviesResult = Result { mockSimilarMovies }
        
        // Perform
        try? await presenter.requestData(movieID: movieID)
        
        // Verify
        XCTAssertEqual(presenter.detailMovie, mockDetailMovie)
        XCTAssertEqual(presenter.creditMovie, mockListCast)
        XCTAssertEqual(presenter.similarMovie, mockSimilarMovies.results)
        XCTAssertTrue(mockView.displayIndicatorCalled)
    }
    
    func testDownloadImageSuccess() async throws {
        let expectedImage = UIImage(named: "image_test")
        mockModel.stubbedGetImageMovieCompletionResult = Result {expectedImage ?? UIImage()}
        let path = "https://image.tmdb.org/t/p/w500/fiVW06jE7z9YnO4trhaMEdclSiC.jpg"
        let downloadedImage = try await presenter.dowloadImage(path: path)
        XCTAssertEqual(downloadedImage, expectedImage)
        XCTAssertEqual(mockModel.mockedDownloadImagePath, path)
    }
    
    func testDownloadImageFailure() async throws {
        mockModel.stubbedGetImageMovieCompletionResult = .failure(MockError.someError)
        let path = "https://image.tmdb.org/t/p/w500/fiVW06jE7z9YnO4trhaMEdclSiC.jpg"
        let downloadedImage = try await presenter.dowloadImage(path: path)
        XCTAssertEqual(downloadedImage, UIImage())
        XCTAssertEqual(mockModel.mockedDownloadImagePath, path)
    }
    
}

// MockDetailMovieModel
class MockDetailMovieModel: DetailMovieContract.Model {
    
    var getDetailMoviesResult: Result<XMovie.ListMovieDetailEntity, Error>?
    var getListCategoryResult: Result<XMovie.ListCategoryResponseEntity, Error>?
    var getSimilarMoviesResult: Result<XMovie.ListMovies, Error>?
    var stubbedGetImageMovieCompletionResult: Result<UIImage, Error>?
    var mockedDownloadImagePath: String?
    
    func getDetailMovies(movieID: Int) async throws -> ListMovieDetailEntity {
        if let result = getDetailMoviesResult {
            return try result.get()
        }
        fatalError("Missing implementation in MockDetailMovieModel")
    }
    
    func getListCategory(movieID: Int) async throws -> ListCategoryResponseEntity {
        if let result = getListCategoryResult {
            return try result.get()
        }
        fatalError("Missing implementation in MockDetailMovieModel")
    }
    
    func getSimilarMovies(movieID: Int, page: Int) async throws -> ListMovies {
        if let result = getSimilarMoviesResult {
            return try result.get()
        }
        fatalError("Missing implementation in MockDetailMovieModel")
    }
    
    func dowloadImage(path: String) async throws -> UIImage {
        if let result = stubbedGetImageMovieCompletionResult {
            mockedDownloadImagePath = path
            switch result {
            case .success(let response):
                return response
            case .failure(let error):
                throw error
            }
        }
        throw MockError.someError
    }
}

// MockDetailMovieView
class MockDetailMovieView: DetailMovieContract.View {
    func reloadCollectionView() {
        
    }
    
    var displayIndicatorCalled = false
    
    func displayIndicator(isDisplay: Bool) {
        displayIndicatorCalled = true
    }
}
