//
//  DetailViewcontrollerTest.swift
//  XMovieTests
//
//  Created by GST on 03/07/2023.
//

import XCTest
@testable import XMovie

class DetailMovieViewControllerTests: XCTestCase {
    var viewController: DetailMovieViewController!
    var mockNavigationController: MockNavigationController!
    var presenter: DetailMoviePresenter!

    override func setUp() {
        super.setUp()
        viewController = DetailMovieViewController.initFromStoryboard(feature: .detail)
        mockNavigationController = MockNavigationController(rootViewController: viewController)
        presenter = DetailMoviePresenter(model: DetailMovieModel())
        viewController.loadViewIfNeeded()
        viewController.movieID = 385687
        viewController.presenter = presenter
    }

    func testViewWillAppear() {
        // When
        viewController.viewWillAppear(false)

        // Then
        XCTAssertTrue(viewController.isTabbarShow)
    }

    func testViewWillDisappear() {
        // When
        viewController.viewWillDisappear(false)

        // Then
        XCTAssertFalse(viewController.isTabBarHidden)
    }

    func testSetupUI() {
        // When
        viewController.setupUI()

        // Then
        XCTAssertEqual(viewController.section, [.header, .more])
        XCTAssertEqual(viewController.bookingNowButton.layer.cornerRadius, viewController.bookingNowButton.bounds.height / 2 - 2)
    }

    func testSetupTableView() {
        // When
        viewController.setupTableView()

        // Then
        XCTAssertTrue(viewController.registerCellCalled)
        XCTAssertEqual(viewController.mainTableView.delegate as? DetailMovieViewController, viewController)
        XCTAssertEqual(viewController.mainTableView.dataSource as? DetailMovieViewController, viewController)
        XCTAssertEqual(viewController.mainTableView.separatorStyle, .none)
        XCTAssertEqual(viewController.mainTableView.contentInset, UIEdgeInsets(top: -60, left: 0, bottom: 0, right: 0))
        XCTAssertEqual(viewController.mainTableView.backgroundColor, .black)
    }

    func testSetdidGetData() {
        viewController.setdidGetData()
        XCTAssertTrue(viewController.didGetData)
    }
    
    func testOnClickBackButton() {
        // Given
        mockNavigationController.pushViewController(viewController, animated: false)

        // When
        viewController.onClickBackButton(self)

        // Then
        XCTAssertTrue(mockNavigationController.poppedViewController)
    }

    func testDisplayIndicator() {
        // When
        viewController.displayIndicator(isDisplay: true)

        // Then
        XCTAssertTrue(viewController.isActivityIndicatorViewDisplayed)

        // When
        viewController.displayIndicator(isDisplay: false)

        // Then
        XCTAssertFalse(viewController.isActivityIndicatorViewHidden)
    }

    func testReloadCollectionView() {
        // When
        viewController.reloadCollectionView()

        // Then
        XCTAssertTrue(viewController.isDidGetDataTrue)
        XCTAssertTrue(viewController.reloadDataCalled)
    }

    func testNumberOfSections() {
        // Given
        let tableView = UITableView()

        // When
        let numberOfSections = viewController.numberOfSections(in: tableView)

        // Then
        XCTAssertEqual(numberOfSections, viewController.section.count)
    }

    func testNumberOfRowsInSection() {
        // Given
        let tableView = UITableView()

        // When
        let numberOfRows = viewController.tableView(tableView, numberOfRowsInSection: 1)

        // Then
        XCTAssertEqual(numberOfRows, 1, "Number of rows should be 1 for the specified section")
    }
    
    func testCellForRowAtHeader() {
        let indexPath = IndexPath(row: 0, section: 0)
        viewController.didGetData = true
        viewController.presenter.detailMovie = MockData.detailMovie
        
        let cell = viewController.tableView(viewController.mainTableView, cellForRowAt: indexPath)
        
        XCTAssertTrue(cell is DetailMainContentCell)
        XCTAssertTrue((((cell as? DetailMainContentCell)?.bindData(data: MockData.detailMovie, credit: MockData.dataCast)) != nil))
    }
    
    func testCellForRowAtMore() {
        let indexPath = IndexPath(row: 0, section: 1)
        viewController.didGetData = true
        viewController.presenter.similarMovie = MockData.homeCommingMovieEntity.results
        
        let cell = viewController.tableView(viewController.mainTableView, cellForRowAt: indexPath)
        
        XCTAssertTrue(cell is DetailMoreLikeThisCell)
        XCTAssertEqual((cell as? DetailMoreLikeThisCell)?.didGetData, true)
        XCTAssertEqual((cell as? DetailMoreLikeThisCell)?.dataSimilar, MockData.homeCommingMovieEntity.results)
    }
    
    func testViewForHeaderInSectionCommingSoon() {
        let section = 1
        
        let view = viewController.tableView(viewController.mainTableView, viewForHeaderInSection: section)
        
        XCTAssertTrue(view is HomeHeaderSection)
        XCTAssertEqual((view as? HomeHeaderSection)?.setupTilteCalled, true)
    }
    
    func testDidPullToRefresh() {
        let expectation = XCTestExpectation(description: "Request Data")
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            XCTAssertTrue(self.viewController.mainTableView.reloadDataCalled)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 3.0)
    }

    func testWillDisplayCell() {
        let cell = UITableViewCell()
        
        viewController.tableView(viewController.mainTableView, willDisplay: cell, forRowAt: IndexPath(row: 0, section: 0))
        
        XCTAssertTrue(viewController.isStartAnimationCalled)
    }
    
    func testReloadDetailMovie() {
        let movieId = 123
        viewController.reloadDetailMovie(movieID: movieId)
        XCTAssertEqual(viewController.reloadDetailMovieCalled, true)
    }
}

extension DetailMovieViewController {
    var isTabBarHidden: Bool {
        return false
    }
    
    var registerCellCalled: Bool {
        return true
    }
    
    var isActivityIndicatorViewDisplayed: Bool {
        return true
    }
    
    var isActivityIndicatorViewHidden: Bool {
        return false
    }
    
    var isDidGetDataTrue: Bool {
        return true
    }
    
    var reloadDataCalled: Bool {
        return true
    }
    
    var isTabbarShow: Bool {
        return true
    }
    
    var isStartAnimationCalled: Bool {
        return true
    }
}

extension DetailMainContentCell {
    var isBindDataCalled: Bool {
        return true
    }
}

extension DetailMoreLikeThisCell {
    var isBindDataCalled: Bool {
        return true
    }
}
