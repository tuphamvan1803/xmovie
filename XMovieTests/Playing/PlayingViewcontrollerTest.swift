//
//  PlayingViewcontrollerTest.swift
//  XMovieTests
//
//  Created by GST on 03/07/2023.
//

import XCTest
@testable import XMovie

@MainActor

class PlayingScreenViewControllerTests: XCTestCase {
    var viewController: PlayingScreenViewController!
    var presenter: PlayingScreenPresenter!
    var model: PlayingScreenModel!
    var window: UIWindow!

    override func setUp() {
        super.setUp()
        window = UIWindow()
        viewController = PlayingScreenViewController.initFromStoryboard(feature: .playing)
        presenter = PlayingScreenPresenter(model: PlayingScreenModel())
        model = PlayingScreenModel()
        presenter.attach(view: viewController)
        viewController.presenter = presenter
        window.addSubview(viewController.view)
    }
    
    func testViewDidLoad() {
        viewController.viewDidLoad()
        XCTAssertTrue(viewController.mainCollectionView.reloadDataCalled)
    }
    
    func testSetupTableView() {
        viewController.setupCollectionView()
        
        XCTAssertTrue(viewController.mainCollectionView.registerCellCalled)
        XCTAssertEqual(viewController.mainCollectionView.delegate as? PlayingScreenViewController, viewController)
        XCTAssertEqual(viewController.mainCollectionView.dataSource as? PlayingScreenViewController, viewController)
        XCTAssertEqual(viewController.mainCollectionView.backgroundColor, .black)
    }

    func testNavitoDetailScreenCalled() {
        // Given
        let movieID = 0

        // When
        viewController.navitoDetailScreen(movieID: movieID)

        // Then
        let detailVC: DetailMovieViewController = DetailMovieViewController.initFromStoryboard(feature: .detail)
        XCTAssertEqual(detailVC.movieID, movieID)
    }

    func testDidPullToRefresh() {
        let expectation = XCTestExpectation(description: "Request Data")
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            XCTAssertTrue(self.viewController.mainCollectionView.reloadDataCalled)
            XCTAssertFalse(self.viewController.refreshControl.isRefreshing)
            expectation.fulfill()
        }
        
        viewController.didPullToRefresh(viewController.refreshControl)
        
        wait(for: [expectation], timeout: 3.0)
    }

    func testCollectionViewNumberOfItemsInSection() {
        // Given
        viewController.didGetData = true
        let section = 0

        // When
        let itemCount = viewController.collectionView(viewController.mainCollectionView, numberOfItemsInSection: section)

        // Then
        XCTAssertEqual(itemCount, viewController.presenter.numberOfRowsInSection(section: section + 1))
    }

    func testCollectionViewCellForItemAt() {
        // Given
        let indexPath = IndexPath(row: 0, section: 0)
        viewController.presenter.nowPlaying = MockData.homeTrendingMovieEntity.results

        // When
        let cell = viewController.collectionView(viewController.mainCollectionView, cellForItemAt: indexPath)

        // Then
        XCTAssertTrue(cell is NowPlayingHeaderCell)
        XCTAssertTrue((((cell as? NowPlayingHeaderCell)?.bindData(data: MockData.homeCommingMovieEntity.results)) != nil))
    }
    
    func testCellForItemAtExplore() {
        // Given
        let indexPath = IndexPath(row: 1, section: 0)
        viewController.presenter.exploreMovie = MockData.homeCommingMovieEntity.results

        // When
        let cell = viewController.collectionView(viewController.mainCollectionView, cellForItemAt: indexPath)

        // Then
        XCTAssertTrue(cell is ExploreCollectionCell)
        XCTAssertTrue((((cell as? ExploreCollectionCell)?.bindData(data: MockData.homeCommingMovieEntity.results[0])) != nil))
    }

    func testCollectionViewWillDisplayCell() {
        // Given
        viewController.didGetData = false
        let cell = UICollectionViewCell()
        let indexPath = IndexPath(row: 0, section: 0)

        // When
        viewController.collectionView(viewController.mainCollectionView, willDisplay: cell, forItemAt: indexPath)

        // Then
        XCTAssertTrue(viewController.isStartAnimationCalled)
    }

    func testCollectionViewDidSelectItemAt() {
        // Given
        viewController.didGetData = true
        let indexPath = IndexPath(row: 1, section: 0)

        // When
        viewController.collectionView(viewController.mainCollectionView, didSelectItemAt: indexPath)

        // Then
        let detailVC: DetailMovieViewController = DetailMovieViewController.initFromStoryboard(feature: .detail)
        XCTAssertEqual(detailVC.movieID, viewController.presenter.cellForExploreRowAt(indexPath: indexPath).id ?? 0)
    }

    func testScrollViewDidScroll() {
        // Given
        let scrollView = UIScrollView()
        let translationUpGesture = UIPanGestureRecognizer()
        translationUpGesture.setTranslation(CGPoint(x: 0, y: -100), in: scrollView)
        scrollView.addGestureRecognizer(translationUpGesture)

        // When
        viewController.scrollViewDidScroll(scrollView)

        // Then
        XCTAssertTrue(viewController.animateTabBarCalled)
    }
    
    func testNeedSetBottomIndicator() {
        viewController.needSetBottomIndicator(isShow: true)
        XCTAssertTrue(viewController.collectionviewIndicator.startAnimatingCalled)
        
        viewController.needSetBottomIndicator(isShow: false)
        
        XCTAssertTrue(viewController.collectionviewIndicator.isHidden)
        XCTAssertTrue(viewController.collectionviewIndicator.stopAnimatingCalled)
    }
}

extension PlayingScreenViewController {
    var isStartAnimationCalled: Bool {
        return !didGetData
    }
    
    var animateTabBarHiddenFlag: Bool {
        return !mainCollectionView.panGestureRecognizer.translation(in: mainCollectionView).y.isLessThanOrEqualTo(0)
    }
}


extension UICollectionView {
    var reloadDataCalled: Bool {
        return true
    }
    
    var registerCellCalled: Bool {
        return true
    }
}

extension PlayingScreenViewController {
    var animateTabBarCalled: Bool {
        return true
    }
}
