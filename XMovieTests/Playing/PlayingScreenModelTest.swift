//
//  PlayingScreenModelTest.swift
//  XMovieTests
//
//  Created by GST on 07/07/2023.
//

import XCTest
@testable import XMovie

final class PlayingScreenModelTest: XCTestCase {
    
    var model: PlayingScreenModel!
    
    override func setUp() {
        super.setUp()
        model = PlayingScreenModel()
    }
    
    override func tearDown() {
        super.tearDown()
        model = nil
    }
    
    func testGetHomeTrendingMovie() async throws {
        do {
            _ = try await model.getExploreMovie(page: 1)
        } catch {}
    }
    
    func testgetHomeCommingMovie() async throws {
        do {
            _ = try await model.getNowPlayingMovie(page: 1)
        } catch {}
    }

    func testdowloadImage() async throws {
        do {
            _ = try await model.dowloadImage(path: "https://image.tmdb.org/t/p/w500/fiVW06jE7z9YnO4trhaMEdclSiC.jpg")
        } catch {}
    }
}
