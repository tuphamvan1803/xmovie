//
//  PlayingHeaderCellTest.swift
//  XMovieTests
//
//  Created by GST on 03/07/2023.
//

import XCTest
@testable import XMovie

class NowPlayingHeaderCellTests: XCTestCase {
    
    var cell: NowPlayingHeaderCell!
    
    override func setUp() {
        super.setUp()
        let nib = UINib(nibName: "NowPlayingHeaderCell", bundle: nil)
        let objects = nib.instantiate(withOwner: nil, options: nil)
        cell = objects.first as? NowPlayingHeaderCell
        cell.awakeFromNib()
    }
    
    override func tearDown() {
        cell = nil
        super.tearDown()
    }
    
    func testBindData() {
        // Given
        let movies = [MovieEntity(title: "Movie 1"), MovieEntity(title: "Movie 2")]
        
        // When
        cell.bindData(data: movies)
        
        // Then
        XCTAssertEqual(cell.dataNowPlaying.count, movies.count)
        XCTAssertTrue(cell.didGetData)
        XCTAssertEqual(cell.mainCollectionView.numberOfItems(inSection: 0), movies.count)
    }
    
    func testCollectionViewDelegate() {
        // Given
        let collectionView = cell.mainCollectionView
        let movies = MockData.homeCommingMovieEntity.results
        cell.dataNowPlaying = movies
        cell.didGetData = true
        collectionView?.reloadData()
        
        // When
        let delegate = cell.collectionView(collectionView! , didSelectItemAt: IndexPath(item: 0, section: 0))
        
        // Then
        XCTAssertTrue(delegate != nil)
    }
    
    func testCollectionViewDataSource() {
        // Given
        let collectionView = cell.mainCollectionView
        let movies = [MovieEntity(title: "Movie 1"), MovieEntity(title: "Movie 2")]
        cell.dataNowPlaying = movies
        cell.didGetData = true
        collectionView?.reloadData()
        
        // When
        let numberOfItems = cell.collectionView(collectionView!, numberOfItemsInSection: 0)
        let cell = cell.collectionView(collectionView!, cellForItemAt: IndexPath(item: 0, section: 0))
        
        // Then
        XCTAssertEqual(numberOfItems, movies.count)
        XCTAssertTrue(cell != nil)
    }
    
    func testCellForRowAt() {
        let indexPath = IndexPath(row: 0, section: 0)
        let collectionView = cell.mainCollectionView
        let movies = [MovieEntity(title: "Movie 1"), MovieEntity(title: "Movie 2")]
        cell.dataNowPlaying = movies
        cell.didGetData = true
        collectionView?.reloadData()
        let cell = cell.collectionView(collectionView!, cellForItemAt: IndexPath(item: 0, section: 0))
        XCTAssertTrue(cell is NowPlayingCollectionCell)
        XCTAssertTrue((((cell as? NowPlayingCollectionCell)?.bindData(data: movies[0])) != nil))
        XCTAssertTrue((((cell as? NowPlayingCollectionCell)?.setupCategoryLabel(data: movies[0])) != nil))
    }
}
