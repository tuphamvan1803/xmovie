//
//  PlayingPresenterTest.swift
//  XMovieTests
//
//  Created by GST on 26/06/2023.
//

import XCTest
@testable import XMovie

class PlayingScreenPresenterTests: XCTestCase {
    var presenter: PlayingScreenPresenter!
    var mockModel: MockPlayingScreenModel!
    var mockView: MockPlayingScreenView!
    
    override func setUp() {
        super.setUp()
        mockModel = MockPlayingScreenModel()
        presenter = PlayingScreenPresenter(model: mockModel)
        mockView = MockPlayingScreenView()
        presenter.attach(view: mockView)
    }
    
    override func tearDown() {
        presenter.detachView()
        super.tearDown()
    }
    
    func testRequestData() async {
        // Prepare
        let nowPlayingMovies = MockData.homeTrendingMovieEntity
        let exploreMovies = MockData.homeCommingMovieEntity
        mockModel.getNowPlayingMovieResult = Result { nowPlayingMovies }
        mockModel.getExploreMovieResult = Result { exploreMovies }
        
        // Perform
        try? await presenter.requestData()
        
        // Verify
        XCTAssertEqual(presenter.nowPlaying, nowPlayingMovies.results)
        XCTAssertEqual(presenter.exploreMovie, exploreMovies.results)
        XCTAssertTrue(mockView.displayIndicatorCalled)
        XCTAssertFalse(mockView.needSetBottomIndicatorCalled)
        XCTAssertTrue(mockView.reloadCollectionViewCalled)
    }
    
    func testRequestData_Failure() async {
        // Given
        mockModel.getNowPlayingMovieResult = .failure(MockError.someError)
        mockModel.getExploreMovieResult = .failure(MockError.someError)

        // When
        do {
            try await presenter.requestData(isRefresh: true)
        } catch {
            // Then
            XCTAssertTrue(mockView.displayIndicatorCalled)
            XCTAssertFalse(presenter.nowPlaying.isEmpty)
            XCTAssertFalse(presenter.exploreMovie.isEmpty)
        }
    }
    
    func testCellForExploreRowAt_LoadMore() async {
        // Prepare
        let mockMovie1 = MockData.homeTrendingMovieEntity
        let mockMovie2 = MockData.homeTrendingMovieEntity
        presenter.exploreMovie.append(contentsOf: mockMovie1.results)
        presenter.exploreMovie.append(contentsOf: mockMovie2.results)
        presenter.pageExplore = 2
        let mockIndexPath = IndexPath(row: presenter.exploreMovie.count - 1, section: 0)
        
        // Set up the expectation for getExploreMovie() call
        mockModel.getExploreMovieResult = Result { mockMovie2 }
        try? await presenter.getExploreMovie()
        
        // Perform
        let result = presenter.cellForExploreRowAt(indexPath: mockIndexPath)
        
        // Verify
        XCTAssertEqual(result, presenter.exploreMovie[mockIndexPath.row])
        XCTAssertEqual(presenter.pageExplore, 2)
        XCTAssertTrue(mockView.reloadCollectionViewCalled)
        
    }
    
    func testDownloadImageSuccess() async throws {
        let expectedImage = UIImage(named: "image_test")
        mockModel.stubbedGetImageMovieCompletionResult = .success(expectedImage ?? UIImage())
        
        let path = "https://image.tmdb.org/t/p/w500/fiVW06jE7z9YnO4trhaMEdclSiC.jpg"
        let downloadedImage = try await presenter.dowloadImage(path: path)
        XCTAssertEqual(downloadedImage, expectedImage)
        XCTAssertEqual(mockModel.mockedDownloadImagePath, path)
    }
    
    func testDownloadImageFailure() async throws {
        mockModel.stubbedGetImageMovieCompletionResult = .failure(MockError.someError)
        let path = "https://image.tmdb.org/t/p/w500/fiVW06jE7z9YnO4trhaMEdclSiC.jpg"
        let downloadedImage = try await presenter.dowloadImage(path: path)
        XCTAssertEqual(downloadedImage, UIImage())
        XCTAssertEqual(mockModel.mockedDownloadImagePath, path)
    }
}

// MockPlayingScreenModel
class MockPlayingScreenModel: PlayingScreenContract.Model {
    var getNowPlayingMovieResult: Result<ListMovies, Error>?
    var getExploreMovieResult: Result<ListMovies, Error>?
    var stubbedGetImageMovieCompletionResult: Result<UIImage, Error>?
    var mockedDownloadImagePath: String?
    
    func getNowPlayingMovie(page: Int) async throws -> ListMovies {
        if let result = getNowPlayingMovieResult {
            return try result.get()
        }
        fatalError("Missing implementation in MockPlayingScreenModel")
    }
    
    func getExploreMovie(page: Int) async throws -> ListMovies {
        if let result = getExploreMovieResult {
            return try result.get()
        }
        fatalError("Missing implementation in MockPlayingScreenModel")
    }
    
    func dowloadImage(path: String) async throws -> UIImage {
        if let result = stubbedGetImageMovieCompletionResult {
            mockedDownloadImagePath = path
            switch result {
            case .success(let response):
                return response
            case .failure(let error):
                throw error
            }
        }
        throw MockError.someError
    }
}

// MockPlayingScreenView
class MockPlayingScreenView: PlayingScreenContract.View {
    func setdidGetData() {
        
    }
    
    var displayIndicatorCalled = false
    var needSetBottomIndicatorCalled = false
    var reloadCollectionViewCalled = true
    
    func displayIndicator(isDisplay: Bool) {
        displayIndicatorCalled = true
    }
    
    func needSetBottomIndicator(isShow: Bool) {
        needSetBottomIndicatorCalled = true
    }
    
    func reloadCollectionView() {
        reloadCollectionViewCalled = true
    }
}
