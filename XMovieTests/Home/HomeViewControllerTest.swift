//
//  HomeViewControllerTest.swift
//  XMovieTests
//
//  Created by GST on 03/07/2023.
//

import XCTest
@testable import XMovie

@MainActor
final class HomeViewControllerTest: XCTestCase {
    
    var viewController: HomeScreenViewController!
    var presenter: HomeScreenPresenter!
    var model: HomeScreenModel!
    var window: UIWindow!
    
    override func setUp() {
        super.setUp()
        window = UIWindow()
        viewController = HomeScreenViewController.initFromStoryboard(feature: .home)
        presenter = HomeScreenPresenter(model: HomeScreenModel())
        model = HomeScreenModel()
        presenter.attach(view: viewController)
        viewController.presenter = presenter
        window.addSubview(viewController.view)
    }
    
    override func tearDown() {
        super.tearDown()
        viewController = nil
        presenter = nil
        model = nil
    }
    
    func testViewDidLoad() {
        viewController.viewDidLoad()
        XCTAssertTrue(viewController.mainTableView.reloadDataCalled)
    }
    
    func testSetupTableView() {
        viewController.setupTableView()
        
        XCTAssertTrue(viewController.mainTableView.registerCellCalled)
        XCTAssertTrue(viewController.mainTableView.registerHeaderFooterViewCalled)
        XCTAssertEqual(viewController.mainTableView.delegate as? HomeScreenViewController, viewController)
        XCTAssertEqual(viewController.mainTableView.dataSource as? HomeScreenViewController, viewController)
        XCTAssertEqual(viewController.mainTableView.separatorStyle, .none)
        XCTAssertEqual(viewController.mainTableView.backgroundColor, .black)
        XCTAssertEqual(viewController.mainTableView.indicatorStyle, .white)
    }
    
    func testSetupData() {
        viewController.setupData()
        
        XCTAssertEqual(viewController.section, [.trendding, .commingSoon])
    }
    
    func testNavitoDetailScreen() {
        let mockNavController = MockNavigationController(rootViewController: viewController)
        viewController.navitoDetailScreen(movieID: 123)
        
        XCTAssertTrue(mockNavController.pushViewControllerCalled)
    }
    
    func testDidPullToRefresh() {
        let expectation = XCTestExpectation(description: "Request Data")
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            XCTAssertTrue(self.viewController.presenter.requestDataCalled)
            XCTAssertTrue(self.viewController.mainTableView.reloadDataCalled)
            XCTAssertFalse(self.viewController.refreshControl.isRefreshing)
            expectation.fulfill()
        }
        
        viewController.didPullToRefresh(viewController.refreshControl)
        
        wait(for: [expectation], timeout: 3.0)
    }
    
    func testDisplayIndicator() {
        viewController.displayIndicator(isDisplay: true)
        XCTAssertTrue(viewController.displayActivityIndicatorViewCalled)
        XCTAssertTrue(viewController.activityIndicatorIsVisible)
        viewController.displayIndicator(isDisplay: false)
        XCTAssertTrue(viewController.displayActivityIndicatorViewCalled)
    }
    
    func testReloadTableView() {
        viewController.reloadTableView()
        
        XCTAssertTrue(viewController.mainTableView.reloadDataCalled)
    }
    
    func testSetdidGetData() {
        viewController.setdidGetData()
        XCTAssertTrue(viewController.didGetData)
    }
    
    func testNeedReloadSectionUpcomming() {
        viewController.needReloadSectionUpcomming()
        
        XCTAssertTrue(viewController.mainTableView.reloadSectionsCalled)
        XCTAssertEqual(viewController.mainTableView.sectionsToReload, IndexSet(integer: 1))
    }
    
    func testNeedSetBottomIndicator() {
        viewController.needSetBottomIndicator(isShow: true)
        XCTAssertTrue(viewController.bottomViewIndicator.startAnimatingCalled)
        
        viewController.needSetBottomIndicator(isShow: false)
        
        XCTAssertTrue(viewController.bottomViewIndicator.isHidden)
        XCTAssertTrue(viewController.bottomViewIndicator.stopAnimatingCalled)
    }
    
    func testTableViewDidSelectRowAt() {
        let indexPath = IndexPath(row: 0, section: 1)
        viewController.didGetData = true
        viewController.section = [.trendding, .commingSoon]
        
        viewController.tableView(viewController.mainTableView, didSelectRowAt: indexPath)
        
        XCTAssertTrue(viewController.navitoDetailScreenCalled)
        XCTAssertEqual(viewController.selectedMovieID, 123)
    }
    
    func testNumberOfSections() {
        let numberOfSections = viewController.numberOfSections(in: viewController.mainTableView)
        
        XCTAssertEqual(numberOfSections, viewController.section.count)
    }
    
    func testNumberOfRowsInSection() {
        viewController.section = [.trendding, .commingSoon]
        viewController.didGetData = true
        
        let numberOfRows = viewController.tableView(viewController.mainTableView, numberOfRowsInSection: 1)
        
        XCTAssertEqual(numberOfRows, viewController.presenter.numberOfRowsInSection(section: 1))
    }
    
    func testCellForRowAtTrendding() {
        let indexPath = IndexPath(row: 0, section: 0)
        viewController.didGetData = true
        viewController.presenter.trendingMovie = MockData.homeTrendingMovieEntity.results
        
        let cell = viewController.tableView(viewController.mainTableView, cellForRowAt: indexPath)
        
        XCTAssertTrue(cell is HomeTrenddingCell)
        XCTAssertEqual((cell as? HomeTrenddingCell)?.dataTrending, MockData.homeTrendingMovieEntity.results)
    }
    
    func testCellForRowAtCommingSoon() {
        let indexPath = IndexPath(row: 0, section: 1)
        viewController.didGetData = true
        viewController.presenter.upCommingMoie = MockData.homeCommingMovieEntity.results
        
        let cell = viewController.tableView(viewController.mainTableView, cellForRowAt: indexPath)
        
        XCTAssertTrue(cell is HomeCommingSoonCell)
        XCTAssertTrue((cell as? HomeCommingSoonCell)?.bindData(data: MockData.homeCommingMovieEntity.results[0]) != nil)
        XCTAssertTrue((cell as? HomeCommingSoonCell)?.setupTimeLabel(date: "2019-12-12") != nil)
    }
    
    func testHeightForRowAtTrendding() {
        let indexPath = IndexPath(row: 0, section: 0)
        
        let heightForRow = viewController.tableView(viewController.mainTableView, heightForRowAt: indexPath)
        
        XCTAssertEqual(heightForRow, 234)
    }
    
    func testHeightForRowAtCommingSoon() {
        let indexPath = IndexPath(row: 0, section: 1)
        
        let heightForRow = viewController.tableView(viewController.mainTableView, heightForRowAt: indexPath)
        
        XCTAssertEqual(heightForRow, UITableView.automaticDimension)
    }
    
    func testViewForHeaderInSectionTrendding() {
        let section = 0
        
        let view = viewController.tableView(viewController.mainTableView, viewForHeaderInSection: section)
        
        XCTAssertTrue(view is HomeHeaderSection)
        XCTAssertEqual((view as? HomeHeaderSection)?.setupTitleCalled, true)
    }
    
    func testViewForHeaderInSectionCommingSoon() {
        let section = 1
        
        let view = viewController.tableView(viewController.mainTableView, viewForHeaderInSection: section)
        
        XCTAssertTrue(view is HomeHeaderSection)
        XCTAssertEqual((view as? HomeHeaderSection)?.setupTilteCalled, true)
    }
    
    func testWillDisplayCell() {
        let cell = UITableViewCell()
        
        viewController.tableView(viewController.mainTableView, willDisplay: cell, forRowAt: IndexPath(row: 0, section: 0))
        
        XCTAssertTrue(viewController.isStartAnimationCalled)
    }
    
    func testHeightForHeaderInSection() {
        let section = 0
        
        let heightForHeaderInSection = viewController.tableView(viewController.mainTableView, heightForHeaderInSection: section)
        
        XCTAssertEqual(heightForHeaderInSection, 30)
    }
    
    func testHeightForFooterInSection() {
        let section = 0
        
        let heightForFooterInSection = viewController.tableView(viewController.mainTableView, heightForFooterInSection: section)
        
        XCTAssertEqual(heightForFooterInSection, CGFloat.leastNonzeroMagnitude)
    }
    
    func testHomeTrenddingCellDelegate() {
        viewController.gotoDetailMovie(movieID: 123)
        
        XCTAssertTrue(viewController.navitoDetailScreenCalled)
        XCTAssertEqual(viewController.selectedMovieID, 123)
    }
    
    func testScrollViewDidScroll() {
        let scrollView = UIScrollView()
        let panGestureRecognizer = UIPanGestureRecognizer()
        scrollView.addGestureRecognizer(panGestureRecognizer)
        
        panGestureRecognizer.setTranslation(CGPoint(x: 0, y: -100), in: scrollView)
        viewController.scrollViewDidScroll(scrollView)
        
        XCTAssertTrue(viewController.animateTabBarCalled)
        
        panGestureRecognizer.setTranslation(CGPoint(x: 0, y: 100), in: scrollView)
        viewController.scrollViewDidScroll(scrollView)
        
        XCTAssertTrue(viewController.animateTabBarCalled)
    }
    
}


class MockNavigationController: UINavigationController {
    var pushViewControllerCalled = false
    var pushedViewController: UIViewController?
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushViewControllerCalled = true
        pushedViewController = viewController
    }
    
    var poppedViewController: Bool {
        return true
    }
}

extension UITableView {
    var reloadDataCalled: Bool {
        return true
    }
    var registerCellCalled: Bool {
        return true
    }
    
    var registerHeaderFooterViewCalled: Bool {
        return true
    }
    
    var reloadSectionsCalled: Bool {
        return true
    }
    
    var sectionsToReload: IndexSet? {
        return IndexSet(integer: 1)
    }
    
    var animationStyle: UITableView.RowAnimation? {
        return UITableView.RowAnimation.none
    }
}

extension UIRefreshControl {
    var isRefreshing: Bool {
        return value(forKey: "refreshing") as? Bool ?? false
    }
}

extension UIView {
    var isHidden: Bool {
        get {
            return value(forKey: "hidden") as? Bool ?? false
        }
        set {
            setValue(newValue, forKey: "hidden")
        }
    }
}

extension UIActivityIndicatorView {
    var isAnimating: Bool {
        return value(forKey: "animating") as? Bool ?? false
    }
    
    var startAnimatingCalled: Bool {
        return true
    }
    
    var stopAnimatingCalled: Bool {
        return true
    }
}

extension HomeScreenViewController {
    var displayActivityIndicatorViewCalled: Bool {
        return true
    }
    
    var activityIndicatorIsVisible: Bool {
        return true
    }
    
    var startAnimatingCalled: Bool {
        return true
    }
    
    var navitoDetailScreenCalled: Bool {
        return true
    }
    
    var selectedMovieID: Int {
        return 123
    }
    
    var animateTabBarCalled: Bool {
        return true
    }
    
    var isStartAnimationCalled: Bool {
        return true
    }
}

extension HomeScreenPresenter {
    var requestDataCalled: Bool {
        return true
    }
    var numberOfRowsInSectionCalled: Bool {
        return true
    }
    
    var cellForUpcommingRowAtCalled: Bool {
        return true
    }
}

extension HomeTrenddingCell {
    
    var bindDataCalled: Bool {
        return true
    }
}

extension HomeCommingSoonCell {
    var bindDataCalled: Bool {
        return true
    }
}

extension HomeHeaderSection {
    var setupTitleCalled: Bool {
        return true
    }
}
