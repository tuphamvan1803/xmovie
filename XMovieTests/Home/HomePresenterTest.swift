//
//  HomePresenterTest.swift
//  XMovieTests
//
//  Created by GST on 26/06/2023.
//

import XCTest
@testable import XMovie


class HomeScreenPresenterTests: XCTestCase {
    var presenter: HomeScreenPresenter!
    var mockView: MockHomeScreenView!
    var mockModel: MockHomeScreenModel!

    override func setUp() {
        super.setUp()
        mockView = MockHomeScreenView()
        mockModel = MockHomeScreenModel()
        presenter = HomeScreenPresenter(model: mockModel)
        presenter.attach(view: mockView)
    }

    override func tearDown() {
        presenter.detachView()
        mockView = nil
        mockModel = nil
        presenter = nil
        super.tearDown()
    }

    func testRequestData_Success() async {
        // Given
        let expectedTrendingMovie = MockData.homeTrendingMovieEntity
        let expectedUpcomingMovie = MockData.homeCommingMovieEntity
        let expectedGenres = MockData.genresEntity
        mockModel.stubbedGetHomeTrendingMovieCompletionResult = .success(expectedTrendingMovie)
        mockModel.stubbedGetHomeCommingMovieCompletionResult = .success(expectedUpcomingMovie)
        AppConfig.shared.stubbedGetDataGenresResult = .success(expectedGenres)

        // When
        do {
            try await presenter.requestData(isRefresh: false)
        } catch {
            XCTFail("Unexpected error occurred: \(error)")
        }

        // Then
        XCTAssertEqual(presenter.trendingMovie, expectedTrendingMovie.results)
        XCTAssertEqual(presenter.upCommingMoie, expectedUpcomingMovie.results)
        XCTAssertEqual(presenter.listgenres.isEmpty, false)
        XCTAssertFalse(mockView.indicatorIsDisplayed)
    }

    func testRequestData_Failure() async {
        // Given
        mockModel.stubbedGetHomeTrendingMovieCompletionResult = .failure(MockError.someError)
        mockModel.stubbedGetHomeCommingMovieCompletionResult = .failure(MockError.someError)
        AppConfig.shared.stubbedGetDataGenresResult = .failure(MockError.someError)

        // When
        do {
            try await presenter.requestData(isRefresh: true)
        } catch {
            // Then
            XCTAssertTrue(mockView.indicatorIsDisplayed)
            XCTAssertFalse(presenter.trendingMovie.isEmpty)
            XCTAssertFalse(presenter.upCommingMoie.isEmpty)
            XCTAssertTrue(presenter.listgenres.isEmpty)
            XCTAssertFalse(mockView.indicatorIsDisplayed)
        }
    }

    func testCellForUpcommingRowAt_LoadMore() {
        // Given
        presenter.pageUpcomming = 2
        let expectedUpcomingMovies = MockData.homeCommingMovieEntity
        presenter.upCommingMoie = expectedUpcomingMovies.results
        let mockContentView = MockHomeScreenView()
        presenter.contentView = mockContentView

        // Then
        XCTAssertEqual(presenter.pageUpcomming, 2)
        XCTAssertEqual(presenter.upCommingMoie.count, expectedUpcomingMovies.results.count)
    }
    
    func testDownloadImageSuccess() async throws {
        // Giả lập việc thành công khi tải hình ảnh
        let expectedImage = UIImage(named: "image_test")
        mockModel.stubbedGetImageMovieCompletionResult = .success(expectedImage ?? UIImage())
        
        // Gọi hàm dowloadImage và kiểm tra kết quả
        let path = "https://image.tmdb.org/t/p/w500/fiVW06jE7z9YnO4trhaMEdclSiC.jpg"
        let downloadedImage = try await presenter.dowloadImage(path: path)
        XCTAssertEqual(downloadedImage, expectedImage)
        XCTAssertEqual(mockModel.mockedDownloadImagePath, path)
    }
    
    func testDownloadImageFailure() async throws {
        // Giả lập việc xảy ra lỗi khi tải hình ảnh
        mockModel.stubbedGetImageMovieCompletionResult = .failure(MockError.someError)
        
        // Gọi hàm dowloadImage và kiểm tra kết quả
        let path = "https://image.tmdb.org/t/p/w500/fiVW06jE7z9YnO4trhaMEdclSiC.jpg"
        let downloadedImage = try await presenter.dowloadImage(path: path)
        XCTAssertEqual(downloadedImage, UIImage())
        XCTAssertEqual(mockModel.mockedDownloadImagePath, path)
    }

}

// MARK: - Mock Classes

class MockHomeScreenView: HomeScreenContract.View {
    func reloadTableView() {
    }
    
    func setdidGetData() {
    }
    
    var indicatorIsDisplayed = false
    var bottomIndicatorIsShown = false

    func displayIndicator(isDisplay: Bool) {
        indicatorIsDisplayed = isDisplay
    }

    func needSetBottomIndicator(isShow: Bool) {
        bottomIndicatorIsShown = isShow
    }

    func needReloadSectionUpcomming() {
        // Perform necessary assertions if needed
    }
}

class MockHomeScreenModel: HomeScreenContract.Model {
 
    var stubbedGetHomeTrendingMovieCompletionResult: Result<ListMovies, Error>?
    var stubbedGetHomeCommingMovieCompletionResult: Result<ListMovies, Error>?
    var stubbedGetImageMovieCompletionResult: Result<UIImage, Error>?
    var mockedDownloadImagePath: String?

    func getHomeTrendingMovie(page: Int) async throws -> ListMovies {
        if let result = stubbedGetHomeTrendingMovieCompletionResult {
            switch result {
            case .success(let response):
                return response
            case .failure(let error):
                throw error
            }
        }
        throw MockError.someError
    }

    func getHomeCommingMovie(page: Int) async throws -> ListMovies {
        if let result = stubbedGetHomeCommingMovieCompletionResult {
            switch result {
            case .success(let response):
                return response
            case .failure(let error):
                throw error
            }
        }
        throw MockError.someError
    }
    
    func dowloadImage(path: String) async throws -> UIImage {
        if let result = stubbedGetImageMovieCompletionResult {
            mockedDownloadImagePath = path
            switch result {
            case .success(let response):
                return response
            case .failure(let error):
                throw error
            }
        }
        throw MockError.someError
    }
}

struct MockResponse<T> {
    let results: [T]
}

enum MockError: Error {
    case someError
}

struct MockGenresResponse {
    let genres: [ListGenresEntity]
}

class AppConfig {
    static let shared = AppConfig()

    var stubbedGetDataGenresResult: Result<ListGenresResponseEntity, Error>?

    func getDataGenres() async throws -> ListGenresResponseEntity {
        if let result = stubbedGetDataGenresResult {
            switch result {
            case .success(let response):
                return response
            case .failure(let error):
                throw error
            }
        }
        throw MockError.someError
    }
}

struct MockData {
    static let homeCommingMovieEntity: ListMovies = .init(page: 1,
                                                          results: [.init(adult: true,
                                                                          backdropPath: nil,
                                                                          genreIDS: [1, 2, 3],
                                                                          id: 123,
                                                                          originalLanguage: "2023-12-12",
                                                                          originalTitle: "Sisu: gia gan bao thu",
                                                                          overview: "1234",
                                                                          popularity: nil,
                                                                          posterPath: nil,
                                                                          releaseDate: "Title",
                                                                          title: "Sisu",
                                                                          video: true,
                                                                          voteAverage: nil,
                                                                          voteCount: 10)],
                                                          totalResults: 20,
                                                          totalPages: 1)
    static let homeTrendingMovieEntity: ListMovies = .init(page: 1,
                                                           results: [.init(adult: true,
                                                                           backdropPath: nil,
                                                                           genreIDS: [1, 2, 3],
                                                                           id: 123,
                                                                           originalLanguage: "2023-12-12",
                                                                           originalTitle: "Sisu: gia gan bao thu",
                                                                           overview: "1234",
                                                                           popularity: nil,
                                                                           posterPath: nil,
                                                                           releaseDate: "Title",
                                                                           title: "Sisu",
                                                                           video: true,
                                                                           voteAverage: nil,
                                                                           voteCount: 10)],
                                                           totalResults: 20,
                                                           totalPages: 1)
    static let genresEntity = ListGenresResponseEntity(genres: [.init(id: 28, name: "Action")])
    
    static let detailMovie = ListMovieDetailEntity(backdropPath: "/4XM8DUTQb3lhLemJC51Jx4a2EuA.jpg",
                                                   genres: [.init(id: 1, name: "Action")],
                                                   homepage: "1",
                                                   originalTitle: "Fast X",
                                                   overview: "",
                                                   posterPath: "",
                                                   id: 385687,
                                                   productionCountries: [.init(iso3166_1: "", name: "")],
                                                   releaseDate: "2023-05-17",
                                                   runtime: 600,
                                                   spokenLanguages: [.init(englishName: "", iso639_1: "", name: "")],
                                                   title: "",
                                                   voteAverage: 9.6)
    
    static let dataCast = ListCategoryResponseEntity(cast: [ListCastResponseEntitty(gender: 1,
                                                                                    id: 123,
                                                                                    knownForDepartment: "",
                                                                                    name: "Nara",
                                                                                    originalName: "Nathan",
                                                                                    profilePath: "",
                                                                                    character: "spact",
                                                                                    job: "")],
                                                     crew: [ListCastResponseEntitty(gender: 2,
                                                                                    id: 321,
                                                                                    knownForDepartment: "",
                                                                                    name: "Dr straint",
                                                                                    originalName: "joth",
                                                                                    profilePath: "",
                                                                                    character: "",
                                                                                    job: "Director")])
}
