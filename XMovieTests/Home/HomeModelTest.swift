//
//  HomeModelTest.swift
//  XMovieTests
//
//  Created by GST on 07/07/2023.
//

import XCTest
@testable import XMovie

final class HomeModelTest: XCTestCase {
    
    var model: HomeScreenModel!
    
    override func setUp() {
        super.setUp()
        model = HomeScreenModel()
    }
    
    override func tearDown() {
        super.tearDown()
        model = nil
    }
    
    func testGetHomeTrendingMovie() async throws {
        do {
            _ = try await model.getHomeTrendingMovie(page: 1)
        } catch {}
    }
    
    func testgetHomeCommingMovie() async throws {
        do {
            _ = try await model.getHomeCommingMovie(page: 1)
        } catch {}
    }

    func testdowloadImage() async throws {
        do {
            _ = try await model.dowloadImage(path: "https://image.tmdb.org/t/p/w500/fiVW06jE7z9YnO4trhaMEdclSiC.jpg")
        } catch {}
    }
}

