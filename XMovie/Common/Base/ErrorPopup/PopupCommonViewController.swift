//
//  PopupCommonViewController.swift
//  XMovie
//
//  Created by GST on 25/06/23.
//

import UIKit

public class PopupCommonViewController: UIViewController {

    @IBOutlet private var containerView: UIView!
    @IBOutlet private weak var parentView: UIView!
    @IBOutlet private weak var mainImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var cancelButton: UIButton!
    @IBOutlet private weak var agreeButton: UIButton!

    public var message: String?
    public var centerTitle: String?
    public var doneAction: (() -> Void)?
    public var isCanTapOutSide: Bool = true
    public var centerFQTitle: String?
    var setupUICalled: Bool = false
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    @IBAction func tapCancelButton(_ sender: UIButton) {
       
    }

    @IBAction func tapAgreeButton(_ sender: UIButton) {
    }
    
    private func setupUI() {
        setupUICalled = true
        if isCanTapOutSide {
            setupTapGuesture()
        }
        messageLabel.text = message
        agreeButton.setTitle("OK", for: .normal)
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.isHidden = true
        titleLabel.text = centerTitle?.isEmpty ?? true ? "Opps!" : centerTitle
        mainImageView.image = #imageLiteral(resourceName: "ic_new_oops")
        
        }

    private func setupTapGuesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(timerAction))
        tap.delegate = self
        self.containerView.addGestureRecognizer(tap)
    }

    @objc
    func timerAction() {
        dismiss(animated: true) {
            self.doneAction?()
        }
    }
    
    func getCancelButton() -> UIButton {
        return cancelButton
    }
    
    func getAgreeButton() -> UIButton {
        return agreeButton
    }
}

extension PopupCommonViewController: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return false
    }
}
