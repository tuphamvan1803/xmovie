//
//  ListMovieResponseEntity.swift
//  XMovie
//
//  Created by GST on 12/06/2023.
//

import Foundation
import CoreData


// MARK: - ListMovieResponseEntity
struct ListMovies: Codable {
    let page: Int
    let results: [MovieEntity]
    let totalResults: Int64?
    let totalPages: Int64?
    
    enum CodingKeys: String, CodingKey {
        case page
        case results
        case totalResults = "total_results"
        case totalPages = "total_pages"
    }
}

// MARK: - Result
struct MovieEntity: Codable, Equatable {
    var adult: Bool?
    var backdropPath: String?
    var genreIDS: [Int]?
    var id: Int?
    var originalLanguage, originalTitle, overview: String?
    var popularity: Double?
    var posterPath, releaseDate, title: String?
    var video: Bool?
    var voteAverage: Double?
    var voteCount: Int?

    private enum CodingKeys: String, CodingKey {
        case adult = "adult"
        case backdropPath = "backdrop_path"
        case genreIDS = "genre_ids"
        case id = "id"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case overview, popularity
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case title = "title"
        case video = "video"
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
    }
}

struct ListMovieRequestEntity {
    static let apiKey = "api_key"
    static let page = "page"
}

struct SearchMovieRequestEntity: Codable {
    let apiKey: String
    let query: String
    let page: Int
    
    private enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
        case query
        case page
    }
    
}

struct DetailMovieRequestEntity: Codable {
    let apiKey: String
    
    private enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
    }
    
}

struct SimilarMovieRequestEntity: Codable {
    let apiKey: String
    let page: Int
    
    private enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
        case page
    }
    
}

struct DetailGenresRequestEntity: Codable {
    let apiKey: String
    let idGenres: Int
    let page: Int
    
    private enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
        case idGenres = "with_genres"
        case page
    }
}
