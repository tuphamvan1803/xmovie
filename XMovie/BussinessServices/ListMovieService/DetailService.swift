//
//  DetailService.swift
//  XMovie
//
//  Created by GST on 24/06/2023.
//

import Foundation
import UIKit

protocol DetailServicesProtocol {
    func getDetailMovies(movieID: Int, completion: @escaping (Bool, ListMovieDetailEntity?, NetworkServiceError) -> ())
    func getListCategory(movieID: Int, completion: @escaping (Bool, ListCategoryResponseEntity?, NetworkServiceError) -> ())
    func getSimilarMovies(movieID: Int, page: Int, completion: @escaping (Bool, ListMovies?, NetworkServiceError) -> ())
    func loadPhoto(_ path: String, completion: @escaping (UIImage?, NetworkServiceError?) -> Void)
}

class DetailServices: DetailServicesProtocol {
    func getDetailMovies(movieID: Int, completion: @escaping (Bool, ListMovieDetailEntity?, NetworkServiceError) -> ()) {
        let detailMovieURL: String = "\(ServerConstants.baseURL)\(ServerConstants.moviePath)\(movieID)"
        let params = [DetailRequestEntity.apiKey : ServerConstants.apiKey]
        APIHelpers.shared.requestGET(url: detailMovieURL, params: params) { success, data in
            if success {
                do {
                    let model = try JSONDecoder().decode(ListMovieDetailEntity.self, from: data!)
                    completion(true, model, .decodeError)
                } catch {
                    completion(false, nil, .decodeError)
                }
            } else {
                completion(false, nil, .serverError)
            }
        }
    }
    
    func getListCategory(movieID: Int, completion: @escaping (Bool, ListCategoryResponseEntity?, NetworkServiceError) -> ()) {
        let detailMovieURL: String = "\(ServerConstants.baseURL)\(ServerConstants.moviePath)\(movieID)/credits"
        let params = [DetailRequestEntity.apiKey : ServerConstants.apiKey]
        APIHelpers.shared.requestGET(url: detailMovieURL, params: params) { success, data in
            if success {
                do {
                    let model = try JSONDecoder().decode(ListCategoryResponseEntity.self, from: data!)
                    completion(true, model, .decodeError)
                } catch {
                    completion(false, nil, .decodeError)
                }
            } else {
                completion(false, nil, .serverError)
            }
        }
    }
    
    func getSimilarMovies(movieID: Int, page: Int, completion: @escaping (Bool, ListMovies?, NetworkServiceError) -> ()) {
        let detailMovieURL: String = "\(ServerConstants.baseURL)\(ServerConstants.moviePath)\(movieID)/similar"
        var params = [ListMovieRequestEntity.apiKey : ServerConstants.apiKey]
        params[ListMovieRequestEntity.page] = "\(page)"
        APIHelpers.shared.requestGET(url: detailMovieURL, params: params) { success, data in
            if success {
                do {
                    let model = try JSONDecoder().decode(ListMovies.self, from: data!)
                    completion(true, model, .decodeError)
                } catch {
                    completion(false, nil, .decodeError)
                }
            } else {
                completion(false, nil, .serverError)
            }
        }
    }
    
    func loadPhoto(_ path: String, completion: @escaping (UIImage?, NetworkServiceError?) -> Void) {
        APIHelpers.shared.downloadImageFromURL(path) { photo, error in
            completion(photo, error)
        }
    }
    
}
