//
//  HomeService.swift
//  XMovie
//
//  Created by GST on 12/06/2023.
//

import Foundation
import UIKit

protocol HomeServicesProtocol {
    func getListHomeMovies(page: Int, type: String, completion: @escaping (Bool, ListMovies?, NetworkServiceError) -> ())
    func getListGenres(completion: @escaping (Bool, ListGenresResponseEntity?, NetworkServiceError) -> ())
    func loadPhoto(_ path: String, completion: @escaping (UIImage?, NetworkServiceError?) -> Void)
}

class HomeServices: HomeServicesProtocol {
    func getListHomeMovies(page: Int, type: String, completion: @escaping (Bool, ListMovies?, NetworkServiceError) -> ()) {
        let trenddingMovieURL: String = "\(ServerConstants.baseURL)\(ServerConstants.moviePath)\(type)"
        var params = [ListMovieRequestEntity.apiKey : ServerConstants.apiKey]
        params[ListMovieRequestEntity.page] = "\(page)"
        APIHelpers.shared.requestGET(url: trenddingMovieURL, params: params) { success, data in
            if success {
                do {
                    let model = try JSONDecoder().decode(ListMovies.self, from: data!)
                    completion(true, model, .decodeError)
                } catch {
                    completion(false, nil, .decodeError)
                }
            } else {
                completion(false, nil, .serverError)
            }
        }
    }
    
    func getListGenres(completion: @escaping (Bool, ListGenresResponseEntity?, NetworkServiceError) -> ()) {
        let trenddingMovieURL: String = "\(ServerConstants.baseURL)\(ServerConstants.genresList)"
        let params = [ListGenresRequestEntity.apiKey : ServerConstants.apiKey]
        APIHelpers.shared.requestGET(url: trenddingMovieURL, params: params) { success, data in
            if success {
                do {
                    let model = try JSONDecoder().decode(ListGenresResponseEntity.self, from: data!)
                    completion(true, model, .noData)
                } catch {
                    completion(false, nil, .noData)
                }
            } else {
                completion(false, nil, .serverError)
            }
        }
    }
    
    func loadPhoto(_ path: String, completion: @escaping (UIImage?, NetworkServiceError?) -> Void) {
        APIHelpers.shared.downloadImageFromURL(path) { photo, error in
            completion(photo, error)
        }
    }
}
