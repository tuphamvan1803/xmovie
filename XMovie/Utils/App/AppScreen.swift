//
//  AppScreen.swift
//  XMovie
//
//  Created by GST on 24/06/2023.
//

import Foundation

public enum AppScreen {
  
    case home
    case playing
    case profile
    case detail

    var storyBoardName: String {
        switch self {
        case .home:
            return "HomeScreen"
        case .playing:
            return "PlayingScreen"
        case .profile:
            return "ProfileScreen"
        case .detail:
            return "DetailMovie"
        }
    }
}
