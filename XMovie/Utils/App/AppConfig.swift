//
//  AppConfig.swift
//  XMovie
//
//  Created by GST on 23/06/2023.
//

import Foundation

class AppConfig {
    
    static let shared: AppConfig = AppConfig()
    private var homeService: HomeServices = HomeServices()
    var listGenres: [ListGenresEntity] = [ListGenresEntity]()
    
    func getDataGenres() async throws -> ListGenresResponseEntity {
        try await withCheckedThrowingContinuation({ continuation in
            homeService.getListGenres(completion: { success, results, error in
                if success, let results {
                    self.listGenres.append(contentsOf: results.genres)
                    continuation.resume(returning: results)
                } else {
                    continuation.resume(throwing: error)
                }
            })
        })
    }
}
