//
//  AvatarTableCell.swift
//  XMovie
//
//  Created by GST on 14/06/2023.
//

import UIKit

class AvatarTableCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        avatarImageView.layer.cornerRadius = avatarImageView.bounds.height/2
    }
}
