//
//  GenaralTableCell.swift
//  XMovie
//
//  Created by GST on 14/06/2023.
//

import UIKit

class GenaralTableCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var notiview: UIView!
    @IBOutlet weak var languageView: UIView!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var logoutView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        containerView.layer.cornerRadius = 12
        containerView.layer.borderColor = UIColor.init(hexString: "1E1E1E").cgColor
        containerView.layer.borderWidth = 1
        messageView.layer.cornerRadius = 12
        languageView.layer.cornerRadius = 12
        notiview.layer.cornerRadius = 12
        logoutView.layer.cornerRadius = 12
    }
}
