//
//  AccountTableCell.swift
//  XMovie
//
//  Created by GST on 14/06/2023.
//

import UIKit

class AccountTableCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var memberView: UIView!
    @IBOutlet weak var changePassView: UIView!
    @IBOutlet weak var editView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        containerView.layer.cornerRadius = 12
        containerView.layer.borderColor = UIColor.init(hexString: "1E1E1E").cgColor
        containerView.layer.borderWidth = 1
        memberView.layer.cornerRadius = 12
        changePassView.layer.cornerRadius = 12
        editView.layer.cornerRadius = 12
    }
    
}
