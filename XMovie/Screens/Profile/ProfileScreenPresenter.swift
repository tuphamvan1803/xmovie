//  
//  ProfileScreenPresenter.swift
//  XMovie
//
//  Created by GST on 08/06/2023.
//

import Foundation

final class ProfileScreenPresenter {
    private weak var contentView: ProfileScreenContract.View?
    private var model: ProfileScreenContract.Model

    init(model: ProfileScreenContract.Model) {
        self.model = model
    }
}

extension ProfileScreenPresenter: ProfileScreenPresenterProtocol {
    func attach(view: ProfileScreenContract.View) {
        contentView = view
    }
}
