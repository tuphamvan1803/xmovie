//  
//  ProfileScreenModel.swift
//  XMovie
//
//  Created by GST on 08/06/2023.
//

import Foundation

enum ProfileScreenSection: Int, CaseIterable {
    case avatar
    case account
    case general
}

struct ProfileScreenViewEntity {
    
}

final class ProfileScreenModel {
    
}

extension ProfileScreenModel: ProfileScreenModelProtocol {
    
}
