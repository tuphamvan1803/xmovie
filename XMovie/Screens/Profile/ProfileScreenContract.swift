//  
//  ProfileScreenContract.swift
//  XMovie
//
//  Created by GST on 08/06/2023.
//

import Foundation

protocol ProfileScreenContract {
    typealias Model = ProfileScreenModelProtocol
    typealias View = ProfileScreenViewProtocol
    typealias Presenter = ProfileScreenPresenterProtocol
}

protocol ProfileScreenModelProtocol {
    
}

protocol ProfileScreenViewProtocol: AnyObject {
        
}

protocol ProfileScreenPresenterProtocol {
    func attach(view: ProfileScreenContract.View)
}
