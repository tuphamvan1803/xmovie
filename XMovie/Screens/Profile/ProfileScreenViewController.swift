//  
//  ProfileScreenViewController.swift
//  XMovie
//
//  Created by GST on 08/06/2023.
//

import UIKit

final class ProfileScreenViewController: UIViewController {
    // MARK: - Variables
    @IBOutlet weak var mainTableView: UITableView!
    var presenter = ProfileScreenPresenter(model: ProfileScreenModel())
    
    // MARK: - IBOutlet
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attach(view: self)
        setupTableView()
    }
    
    func setupTableView() {
        mainTableView.registerCell(AvatarTableCell.self)
        mainTableView.registerCell(AccountTableCell.self)
        mainTableView.registerCell(GenaralTableCell.self)
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.separatorStyle = .none
        mainTableView.backgroundColor = .black
    }
}

extension ProfileScreenViewController {
    
}

extension ProfileScreenViewController: ProfileScreenViewProtocol {
    
}

// MARK: - UITableViewDelegate
extension ProfileScreenViewController: UITableViewDelegate {
    
}

// MARK: - UITableViewDataSource
extension ProfileScreenViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        ProfileScreenSection.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case ProfileScreenSection.avatar.rawValue:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: AvatarTableCell.className, for: indexPath) as? AvatarTableCell else {
                return UITableViewCell()
            }
            return cell
        case ProfileScreenSection.account.rawValue:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: AccountTableCell.className, for: indexPath) as? AccountTableCell else {
                return UITableViewCell()
            }
            return cell
        case ProfileScreenSection.general.rawValue:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: GenaralTableCell.className, for: indexPath) as? GenaralTableCell else {
                return UITableViewCell()
            }
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
