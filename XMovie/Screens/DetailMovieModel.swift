//  
//  DetailMovieModel.swift
//  XMovie
//
//  Created by GST on 24/06/2023.
//

import Foundation
import UIKit

enum DetailMovieSection: Int {
    case header
    case more
}

final class DetailMovieModel {
    private var detailService: DetailServices = DetailServices()
}

extension DetailMovieModel: DetailMovieModelProtocol {
    func getDetailMovies(movieID: Int) async throws -> ListMovieDetailEntity {
        try await withCheckedThrowingContinuation({ continuation in
            detailService.getDetailMovies(movieID: movieID, completion: { success, results, error in
                if success, let results {
                    continuation.resume(returning: results)
                } else {
                    continuation.resume(throwing: error)
                }
            })
        })
    }
    
    func getListCategory(movieID: Int) async throws -> ListCategoryResponseEntity {
        try await withCheckedThrowingContinuation({ continuation in
            detailService.getListCategory(movieID: movieID, completion: { success, results, error in
                if success, let results {
                    continuation.resume(returning: results)
                } else {
                    continuation.resume(throwing: error)
                }
            })
        })
    }
    
    func getSimilarMovies(movieID: Int, page: Int) async throws -> ListMovies {
        try await withCheckedThrowingContinuation({ continuation in
            detailService.getSimilarMovies(movieID: movieID, page: page, completion: { success, results, error in
                if success, let results {
                    continuation.resume(returning: results)
                } else {
                    continuation.resume(throwing: error)
                }
            })
        })
    }
    
    func dowloadImage(path: String) async throws -> UIImage {
        try await withCheckedThrowingContinuation({ continuation in
            detailService.loadPhoto(path) { image, error in
                if let error = error {
                    continuation.resume(throwing: error)
                }
                if let image = image {
                    continuation.resume(returning: image)
                }
            }
        })
    }
}
