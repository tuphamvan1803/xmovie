//
//  DetailMoreLikeThisCell.swift
//  XMovie
//
//  Created by GST on 24/06/2023.
//

import UIKit

protocol DetailMoreLikeThisCellDelegate: AnyObject {
    func reloadDetailMovie(movieID: Int)
    func didLoadBackdropImage(path: String) async throws -> UIImage?
}

class DetailMoreLikeThisCell: UITableViewCell {
    
    weak var delegate: DetailMoreLikeThisCellDelegate?
    var dataSimilar: [MovieEntity] = [MovieEntity]()
    var didGetData: Bool = false

    @IBOutlet weak var mainCollectionView: UICollectionView!
    @IBOutlet weak var heightOfCollectionVIewContraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        setupCollectionView()
        dataSimilar = []
    }
    
    func bindData(data: [MovieEntity]) {
        dataSimilar = data
        heightOfCollectionVIewContraint.constant = CGFloat((290 + 16) * data.count / 3)
        mainCollectionView.reloadData()
        layoutIfNeeded()
    }

    private func setupCollectionView() {
        mainCollectionView.registerCell(ExploreCollectionCell.self)
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
    }
}
 
extension DetailMoreLikeThisCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if didGetData {
            delegate?.reloadDetailMovie(movieID: dataSimilar[indexPath.row].id ?? 0)
        }
    }
}

extension DetailMoreLikeThisCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if didGetData {
            return dataSimilar.count
        } else {
            return 10
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ExploreCollectionCell.className, for: indexPath) as? ExploreCollectionCell else {return UICollectionViewCell() }
        if didGetData {
            cell.configShimmerCell(isShimmer: false)
            cell.bindData(data: dataSimilar[indexPath.row])
            cell.delegate = self
        } else {
            cell.configShimmerCell(isShimmer: true)
        }
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension DetailMoreLikeThisCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        // margin left, center and right
        return CGSize(width: (collectionView.frame.width - AppConstants.margin * 2.0) / 3.0, height: 230)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: AppConstants.margin / 2, bottom: AppConstants.margin, right: AppConstants.margin / 2)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        8
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
       8
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
        .zero
    }
}

extension DetailMoreLikeThisCell: ExploreCollectionCellDelegate {
    func didLoadBackdropImage(path: String) async throws -> UIImage? {
        try await delegate?.didLoadBackdropImage(path: path)
    }
}
