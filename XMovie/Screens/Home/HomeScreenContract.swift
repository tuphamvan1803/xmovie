//  
//  HomeScreenContract.swift
//  XMovie
//
//  Created by GST on 08/06/2023.
//

import Foundation
import UIKit

protocol HomeScreenContract {
    typealias Model = HomeScreenModelProtocol
    typealias View = HomeScreenViewProtocol
    typealias Presenter = HomeScreenPresenterProtocol
}

protocol HomeScreenModelProtocol {
    func getHomeTrendingMovie(page: Int) async throws -> ListMovies 
    func getHomeCommingMovie(page: Int) async throws -> ListMovies
    func dowloadImage(path: String) async throws -> UIImage
}

protocol HomeScreenViewProtocol: AnyObject {
    func displayIndicator(isDisplay: Bool)
    func reloadTableView()
    func setdidGetData()
    func needReloadSectionUpcomming()
    func needSetBottomIndicator(isShow: Bool)
}

protocol HomeScreenPresenterProtocol {
    func attach(view: HomeScreenContract.View)
    func detachView()
    func requestData(isRefresh: Bool) async throws
    func numberOfRowsInSection(section: Int) -> Int
    func cellForUpcommingRowAt(indexPath: IndexPath) -> MovieEntity
    func dowloadImage(path: String) async throws -> UIImage
}
