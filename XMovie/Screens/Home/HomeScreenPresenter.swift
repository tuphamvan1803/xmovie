//  
//  HomeScreenPresenter.swift
//  XMovie
//
//  Created by GST on 08/06/2023.
//

import Foundation
import UIKit

final class HomeScreenPresenter {
    weak var contentView: HomeScreenContract.View?
    private var model: HomeScreenContract.Model
    var pageUpcomming: Int = 1
    var pageTrendding: Int = 1
    var trendingMovie: [MovieEntity] = [MovieEntity]()
    var upCommingMoie: [MovieEntity] = [MovieEntity]()
    var listgenres: [ListGenresEntity] = [ListGenresEntity]()
    
    init(model: HomeScreenContract.Model) {
        self.model = model
    }
}

extension HomeScreenPresenter: HomeScreenPresenterProtocol {
    func attach(view: HomeScreenContract.View) {
        contentView = view
    }
    
    func detachView() {
        contentView = nil
    }
    
    func requestData(isRefresh: Bool = false) async throws {
        if isRefresh {
            pageUpcomming = 1
            trendingMovie.removeAll()
            upCommingMoie.removeAll()
            listgenres.removeAll()
        }
        do {
            contentView?.displayIndicator(isDisplay: true)
            trendingMovie.append(contentsOf: try await model.getHomeTrendingMovie(page: pageTrendding).results)
            upCommingMoie.append(contentsOf: try await model.getHomeCommingMovie(page: pageUpcomming).results)
            listgenres.append(contentsOf: try await AppConfig.shared.getDataGenres().genres)
            contentView?.displayIndicator(isDisplay: false)
        } catch {
            print("GetDataError")
            contentView?.displayIndicator(isDisplay: false)
        }
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return upCommingMoie.count
    }

    func cellForUpcommingRowAt(indexPath: IndexPath) -> MovieEntity {
        // Check if the last row number is the same as the last current data element
            if indexPath.row == self.upCommingMoie.count - 1 {
                self.pageUpcomming += 1
                contentView?.needSetBottomIndicator(isShow: true)
                Task {
                    let moreMovie = try await model.getHomeCommingMovie(page: pageUpcomming).results
                    upCommingMoie.append(contentsOf: moreMovie)
                    contentView?.needReloadSectionUpcomming()
                }
                contentView?.needSetBottomIndicator(isShow: false)
            }
        return upCommingMoie.count == 0 ? MovieEntity() : upCommingMoie[indexPath.row]
    }
    
    func dowloadImage(path: String) async throws -> UIImage {
        do {
            return try await model.dowloadImage(path: path)
        } catch {
            return UIImage()
        }
    }
}
