//  
//  HomeScreenModel.swift
//  XMovie
//
//  Created by GST on 08/06/2023.
//

import Foundation
import UIKit

enum HomeScreenSection: CaseIterable {
    case trendding
    case commingSoon
}

final class HomeScreenModel {
    private var homeService: HomeServices = HomeServices()
}

extension HomeScreenModel: HomeScreenModelProtocol {
    func getHomeTrendingMovie(page: Int) async throws -> ListMovies {
        try await withCheckedThrowingContinuation({ continuation in
            homeService.getListHomeMovies(page: page, type: Contants.popularType, completion: { success, results, error in
                if success, let results {
                    continuation.resume(returning: results)
                } else {
                    continuation.resume(throwing: error)
                }
            })
        })
    }
    
    func getHomeCommingMovie(page: Int) async throws -> ListMovies {
        try await withCheckedThrowingContinuation({ continuation in
            homeService.getListHomeMovies(page: page, type: Contants.upcommingType, completion: { success, results, error in
                if success, let results {
                    continuation.resume(returning: results)
                } else {
                    continuation.resume(throwing: error)
                }
            })
        })
    }
    
    func dowloadImage(path: String) async throws -> UIImage {
        try await withCheckedThrowingContinuation({ continuation in
            homeService.loadPhoto(path) { image, error in
                if let error = error {
                    continuation.resume(throwing: error)
                }
                if let image = image {
                    continuation.resume(returning: image)
                }
            }
        })
    }
}
