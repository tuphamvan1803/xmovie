//
//  HomeHeaderSection.swift
//  XMovie
//
//  Created by GST on 12/06/2023.
//

import UIKit

class HomeHeaderSection: UITableViewHeaderFooterView {

    @IBOutlet private var mainView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    var setupTilteCalled: Bool = false
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func setupTitle(title: String) {
        titleLabel.text = title
        setupTilteCalled = true
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed(HomeHeaderSection.className, owner: self, options: nil)
        addSubview(mainView)
        mainView.frame = self.bounds
        mainView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
}
