//
//  HomeCommingSoonCell.swift
//  XMovie
//
//  Created by GST on 12/06/2023.
//

import UIKit

protocol HomeCommingSoonCellDelegate: AnyObject {
    func didLoadBackdropImage(path: String) async throws -> UIImage?
}

class HomeCommingSoonCell: UITableViewCell {
    
    weak var delegate: HomeCommingSoonCellDelegate?
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var contentImage: UIImageView!
    @IBOutlet private weak var monthLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        contentImage.layer.cornerRadius = 12
        contentImage.layer.borderWidth = 1
        contentImage.layer.borderColor = ColorDefine.color1E1E1E.cgColor
    }
    
    func bindData(data: MovieEntity) {
        titleLabel.text = data.title
        contentImage.image = nil
        Task {
            contentImage.image = try await delegate?.didLoadBackdropImage(path: "\(Contants.imageUrl)\(data.backdropPath ?? .empty)")
        }
        setupTimeLabel(date: data.releaseDate ?? AppConstants.empty)
    }
    
    func setupTimeLabel(date: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Contants.dateFormatyyyyMMdd
        guard let date = dateFormatter.date(from: date) else {
            return
        }

        let calendar = Calendar.current
        let day = calendar.component(.day, from: date)
        let monthFormatter = DateFormatter()
        monthFormatter.dateFormat = Contants.monthFormatMMM
        let month = monthFormatter.string(from: date)
        monthLabel.text = month
        dateLabel.text = "\(day)"
    }
}
