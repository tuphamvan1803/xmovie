//
//  PageTrenddingCollectionViewCell.swift
//  XMovie
//
//  Created by GST on 12/06/2023.
//

import UIKit

protocol PageTrenddingCollectionViewCellDelegate: AnyObject {
    func didLoadBackdropImage(path: String) async throws -> UIImage?
}

class PageTrenddingCollectionViewCell: UICollectionViewCell {
    
    weak var delegate: PageTrenddingCollectionViewCellDelegate?

    @IBOutlet private weak var contentImageView: UIImageView!
    @IBOutlet private weak var titleHeaderLabel: UILabel!
    @IBOutlet private weak var categoryLabel: UILabel!
    @IBOutlet private weak var yearPublishLabel: UILabel!
    @IBOutlet private weak var totalTimeLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet weak var opacityView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentImageView.layer.cornerRadius = 12
        contentImageView.layer.borderWidth = 1
        contentImageView.layer.borderColor = ColorDefine.color1E1E1E.cgColor
        addHalfGradientToView(view: opacityView)
    }
    
    func bindData(data: MovieEntity) {
        titleHeaderLabel.text = data.originalTitle
        setupCategoryLabel(data: data)
        totalTimeLabel.text = "\(data.voteAverage ?? 0)"
        contentImageView.image = nil
        Task {
            contentImageView.image = try await delegate?.didLoadBackdropImage(path: "\(Contants.imageUrl)\(data.backdropPath ?? .empty)")
        }
    }
    
    func setupCategoryLabel(data: MovieEntity) {
//        if data.genreIDS?.count ?? 0 > 3 {
//            let dataGenres = data.genreIDS?.prefix(3)
//            let namesArray = dataGenres?.compactMap { id -> String? in
//                if let genre = AppConfig.shared.listGenres.first(where: { $0.id == id }) {
//                    return genre.name
//                }
//                return nil
//            }
//            categoryLabel.text = namesArray?.joined(separator: ", ")
//        } else {
            let namesArray = data.genreIDS?.compactMap { id -> String? in
                if let genre = AppConfig.shared.listGenres.first(where: { $0.id == id }) {
                    return genre.name
                }
                return nil
            }
            categoryLabel.text = namesArray?.joined(separator: ", ")
//        }
    }
}
