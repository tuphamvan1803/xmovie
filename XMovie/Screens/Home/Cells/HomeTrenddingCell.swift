//
//  HomeTrenddingCell.swift
//  XMovie
//
//  Created by GST on 12/06/2023.
//

import UIKit

protocol HomeTrenddingCellDelegate: AnyObject {
    func gotoDetailMovie(movieID: Int)
    func didLoadPosterImage(path: String) async throws -> UIImage?
}

class HomeTrenddingCell: UITableViewCell {

    let flowLayout = ZoomAndSnapFlowLayout()
    weak var delegate: HomeTrenddingCellDelegate?
    var dataTrending: [MovieEntity] = [MovieEntity]()
    var didGetData: Bool = false
    
    @IBOutlet private weak var pageControll: UIPageControl!
    @IBOutlet private weak var pageControllView: UIView!
    @IBOutlet private weak var mainCollectionView: UICollectionView!
    @IBOutlet private weak var collectionviewLeftContraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCollectionView()
        collectionviewLeftContraint.constant = -20
        dataTrending = []
    }
    
    func bindData(data: [MovieEntity]) {
        self.dataTrending = data
        pageControll.numberOfPages = data.count
        mainCollectionView.reloadData()
    }
    
    private func setupCollectionView() {
        mainCollectionView.registerCell(PageTrenddingCollectionViewCell.self)
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        mainCollectionView.collectionViewLayout = flowLayout
        mainCollectionView.contentInsetAdjustmentBehavior = .always
        pageControll.numberOfPages = dataTrending.count
        pageControll.currentPage = 0 
        pageControll.pageIndicatorTintColor = .gray
        pageControll.currentPageIndicatorTintColor = .white
    }
}

extension HomeTrenddingCell: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
}

extension HomeTrenddingCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if didGetData {
            return dataTrending.count
        } else {
            return 10
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PageTrenddingCollectionViewCell.className, for: indexPath) as? PageTrenddingCollectionViewCell else {return UICollectionViewCell() }
        if didGetData {
            cell.bindData(data: dataTrending[indexPath.row])
            cell.delegate = self
        } else {
        }
        return cell
    }
}

extension HomeTrenddingCell: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let collectionView = mainCollectionView else { return }
        let visibleCenterX = collectionView.contentOffset.x + collectionView.bounds.width / 2
        if let indexPath = collectionView.indexPathForItem(at: CGPoint(x: visibleCenterX, y: collectionView.bounds.height / 2)) {
            let pageIndex = indexPath.item
            pageControll.currentPage = pageIndex
            if pageIndex == 0 {
                collectionviewLeftContraint.constant = -20
                layoutIfNeeded()
            } else {
                collectionviewLeftContraint.constant = 0
                layoutIfNeeded()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if didGetData {
            delegate?.gotoDetailMovie(movieID: dataTrending[indexPath.row].id ?? 0)
        }
    }
}

extension HomeTrenddingCell: PageTrenddingCollectionViewCellDelegate {
    func didLoadBackdropImage(path: String) async throws -> UIImage? {
        try await delegate?.didLoadPosterImage(path: path)
    }
}
