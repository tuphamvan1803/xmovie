//  
//  HomeScreenViewController.swift
//  XMovie
//
//  Created by GST on 08/06/2023.
//

import UIKit

final class HomeScreenViewController: BaseViewController {
    // MARK: - Variables
    var presenter = HomeScreenPresenter(model: HomeScreenModel())
    var section: [HomeScreenSection] = []
    let refreshControl = UIRefreshControl()
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var bottomViewIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attach(view: self)
        setupData()
        setupTableView()
        Task {
           try await presenter.requestData()
            didGetData = true
            mainTableView.reloadData()
        }
    }
    
    func setupTableView() {
        bottomViewIndicator.isHidden = true
        mainTableView.registerCell(HomeCommingSoonCell.self)
        mainTableView.registerCell(HomeTrenddingCell.self)
        mainTableView.register(HomeHeaderSection.self, forHeaderFooterViewReuseIdentifier: HomeHeaderSection.className)
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.separatorStyle = .none
        mainTableView.backgroundColor = .black
        mainTableView.indicatorStyle = .white
        refreshControl.addTarget(self, action: #selector(didPullToRefresh(_:)), for: .valueChanged)
        mainTableView.refreshControl = refreshControl
    }
    
    func setupData() {
        section = [.trendding, .commingSoon]
    }
    
    func navitoDetailScreen(movieID: Int) {
        let detailVC: DetailMovieViewController = DetailMovieViewController.initFromStoryboard(feature: .detail)
        detailVC.movieID = movieID
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    @objc func didPullToRefresh(_ sender: Any) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            Task {
                try await self.presenter.requestData(isRefresh: true)
                self.refreshControl.endRefreshing()
                self.mainTableView.reloadData()
            }
        }
    }
}

extension HomeScreenViewController: HomeScreenViewProtocol {
    func displayIndicator(isDisplay: Bool) {
        DispatchQueue.main.async {
            self.displayActivityIndicatorView(display: isDisplay)
        }
    }
    
    func reloadTableView() {
        DispatchQueue.main.async {
            self.mainTableView.reloadData()
        }
    }
    
    func setdidGetData() {
        didGetData = true
    }
    
    func needReloadSectionUpcomming() {
        DispatchQueue.main.async {
            UIView.setAnimationsEnabled(false)
            self.mainTableView.beginUpdates()
            self.mainTableView.reloadSections([1], with: .none)
            self.mainTableView.endUpdates()
            UIView.setAnimationsEnabled(true)
        }
    }
    
    func needSetBottomIndicator(isShow: Bool) {
        DispatchQueue.main.async {
            self.bottomViewIndicator.isHidden = !isShow
            if isShow {
                self.bottomViewIndicator.startAnimating()
            } else {
                self.bottomViewIndicator.stopAnimating()
            }
        }
    }
}

// MARK: - UITableViewDelegate
extension HomeScreenViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch self.section[indexPath.section] {
        case .trendding:
            break
        case .commingSoon:
            navitoDetailScreen(movieID: presenter.cellForUpcommingRowAt(indexPath: indexPath).id ?? 0)
        }
    }
}

// MARK: - UITableViewDataSource
extension HomeScreenViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return section.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.section[section] {
        case .trendding:
            return 1
        case .commingSoon:
            if didGetData {
                return presenter.numberOfRowsInSection(section: section)
            } else {
                return 10
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.section[indexPath.section] {
        case .trendding:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: HomeTrenddingCell.className, for: indexPath) as? HomeTrenddingCell else {
                return UITableViewCell()
            }
            if didGetData {
                cell.didGetData = true
                cell.bindData(data: presenter.trendingMovie)
                cell.delegate = self
            } else {
                cell.didGetData = false
            }
            return cell
        case .commingSoon:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: HomeCommingSoonCell.className, for: indexPath) as? HomeCommingSoonCell else {
                return UITableViewCell()
            }
            if didGetData {
                cell.bindData(data: presenter.cellForUpcommingRowAt(indexPath: indexPath))
                cell.delegate = self
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch self.section[indexPath.section] {
        case .trendding:
            return 234
        case .commingSoon:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch self.section[section] {
        case .trendding:
            guard let headerViewCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: HomeHeaderSection.className) as? HomeHeaderSection else {
                return UIView()
            }
            headerViewCell.setupTitle(title: "Trending")
            return headerViewCell
        case .commingSoon:
            guard let headerViewCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: HomeHeaderSection.className) as? HomeHeaderSection else {
                return UIView()
            }
            headerViewCell.setupTitle(title: "CommingSoon")
            return headerViewCell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !didGetData {
            isStartAnimation(isStart: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
}

extension HomeScreenViewController: HomeTrenddingCellDelegate {
    func didLoadPosterImage(path: String) async throws -> UIImage? {
        try await presenter.dowloadImage(path: path)
    }
    
    func gotoDetailMovie(movieID: Int) {
        navitoDetailScreen(movieID: movieID)
    }
}

extension HomeScreenViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.panGestureRecognizer.translation(in: scrollView).y < 0 {
            animateTabBar(hidden: true)
        } else {
            animateTabBar(hidden: false)
        }
    }
}

extension HomeScreenViewController: HomeCommingSoonCellDelegate {
    func didLoadBackdropImage(path: String) async throws -> UIImage? {
        try await presenter.dowloadImage(path: path)
    }
}
