//  
//  DetailMoviePresenter.swift
//  XMovie
//
//  Created by GST on 24/06/2023.
//

import Foundation
import UIKit

final class DetailMoviePresenter {
    private weak var contentView: DetailMovieContract.View?
    private var model: DetailMovieContract.Model
    var page: Int = 1
    var similarMovie: [MovieEntity] = [MovieEntity]()
    var creditMovie: ListCategoryResponseEntity?
    var detailMovie: ListMovieDetailEntity?

    init(model: DetailMovieContract.Model) {
        self.model = model
    }
}

extension DetailMoviePresenter: DetailMoviePresenterProtocol {
    func attach(view: DetailMovieContract.View) {
        contentView = view
    }
    
    func detachView() {
        contentView = nil
    }
    
    func requestData(movieID: Int) async throws {
        self.similarMovie.removeAll()
        do {
            contentView?.displayIndicator(isDisplay: true)
            detailMovie = try await model.getDetailMovies(movieID: movieID)
            creditMovie = try await model.getListCategory(movieID: movieID)
            similarMovie.append(contentsOf: try await model.getSimilarMovies(movieID: movieID, page: 1).results)
            contentView?.reloadCollectionView()
            contentView?.displayIndicator(isDisplay: false)
        } catch {
            print("GetDataError")
            contentView?.displayIndicator(isDisplay: false)
        }
    }
    
    func dowloadImage(path: String) async throws -> UIImage {
        do {
            return try await model.dowloadImage(path: path)
        } catch {
            return UIImage()
        }
    }
}
