//  
//  DetailMovieContract.swift
//  XMovie
//
//  Created by GST on 24/06/2023.
//

import Foundation
import UIKit

protocol DetailMovieContract {
    typealias Model = DetailMovieModelProtocol
    typealias View = DetailMovieViewProtocol
    typealias Presenter = DetailMoviePresenterProtocol
}

protocol DetailMovieModelProtocol {
    func getDetailMovies(movieID: Int) async throws -> ListMovieDetailEntity
    func getListCategory(movieID: Int) async throws -> ListCategoryResponseEntity
    func getSimilarMovies(movieID: Int, page: Int) async throws -> ListMovies
    func dowloadImage(path: String) async throws -> UIImage
}

protocol DetailMovieViewProtocol: AnyObject {
    func displayIndicator(isDisplay: Bool)
    func reloadCollectionView()
}

protocol DetailMoviePresenterProtocol {
    func attach(view: DetailMovieContract.View)
    func detachView()
}
