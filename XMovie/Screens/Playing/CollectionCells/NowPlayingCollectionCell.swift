//
//  NowPlayingCollectionCell.swift
//  XMovie
//
//  Created by GST on 13/06/2023.
//

import UIKit

protocol NowPlayingCollectionCellDelegate: AnyObject {
    func didLoadBackdropImage(path: String) async throws -> UIImage?
}

class NowPlayingCollectionCell: UICollectionViewCell {
    
    weak var delegate: NowPlayingCollectionCellDelegate?

    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var bookMovieButton: UIButton!
    @IBOutlet weak var voteAvergerLabel: UILabel!
    @IBOutlet weak var starImage: UIImageView!
    @IBOutlet weak var opacityView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bookMovieButton.layer.cornerRadius = 16
        contentImageView.layer.cornerRadius = 16
        contentImageView.layer.borderWidth = 1
        contentImageView.layer.borderColor = ColorDefine.color1E1E1E.cgColor
        configShimmerCell(isShimmer: true)
        addHalfGradientToView(view: opacityView)
    }
    
    func configShimmerCell(isShimmer: Bool = false) {
        if !isShimmer {
            self.stopShimmeringAnimation()
            contentImageView.stopShimmeringAnimation()
            titleLabel.stopShimmeringAnimation()
            categoryLabel.stopShimmeringAnimation()
            bookMovieButton.stopShimmeringAnimation()
            voteAvergerLabel.stopShimmeringAnimation()
            starImage.stopShimmeringAnimation()
        } else {
            contentImageView.startShimmeringAnimation()
            titleLabel.startShimmeringAnimation()
            categoryLabel.startShimmeringAnimation()
            bookMovieButton.startShimmeringAnimation()
            voteAvergerLabel.startShimmeringAnimation()
            starImage.startShimmeringAnimation()
        }
    }

    func bindData(data: MovieEntity) {
        titleLabel.text = data.originalTitle
        voteAvergerLabel.text = "\(data.voteAverage ?? 0)"
        contentImageView.image = nil
        Task {
            contentImageView.image = try await delegate?.didLoadBackdropImage(path: "\(Contants.imageUrl)\(data.backdropPath ?? .empty)")
        }
        setupCategoryLabel(data: data)
        bookMovieButton.setTitle("  Book now  ", for: .normal)
    }
    
    func setupCategoryLabel(data: MovieEntity) {
        if data.genreIDS?.count ?? 0 > 3 {
            let dataGenres = data.genreIDS?.prefix(3)
            let namesArray = dataGenres?.compactMap { id -> String? in
                if let genre = AppConfig.shared.listGenres.first(where: { $0.id == id }) {
                    return genre.name
                }
                return nil
            }
            categoryLabel.text = namesArray?.joined(separator: ", ")
        } else {
            let namesArray = data.genreIDS?.compactMap { id -> String? in
                if let genre = AppConfig.shared.listGenres.first(where: { $0.id == id }) {
                    return genre.name
                }
                return nil
            }
            categoryLabel.text = namesArray?.joined(separator: ", ")
        }
    }
}
