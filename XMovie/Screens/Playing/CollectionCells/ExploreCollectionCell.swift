//
//  ExploreCollectionCell.swift
//  XMovie
//
//  Created by GST on 14/06/2023.
//

import UIKit

protocol ExploreCollectionCellDelegate: AnyObject {
    func didLoadBackdropImage(path: String) async throws -> UIImage?
}

class ExploreCollectionCell: UICollectionViewCell {
    
    weak var delegate: ExploreCollectionCellDelegate?

    @IBOutlet private weak var backgroundImageView: UIImageView!
    @IBOutlet private weak var totalLikeLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var likeView: UIView!
    @IBOutlet private weak var opacityView: UIView!
    @IBOutlet private weak var circalView: CircleView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundImageView.layer.cornerRadius = 8
        backgroundImageView.layer.borderWidth = 1
        likeView.layer.cornerRadius = 4
        backgroundImageView.layer.borderColor = ColorDefine.color1E1E1E.cgColor
        addGradientTopToBotView(view: opacityView)
    }
    
    func configShimmerCell(isShimmer: Bool = false) {
        if !isShimmer {
            self.stopShimmeringAnimation()
            backgroundImageView.stopShimmeringAnimation()
            titleLabel.stopShimmeringAnimation()
            timeLabel.stopShimmeringAnimation()
            likeView.stopShimmeringAnimation()
            circalView.stopShimmeringAnimation()
        } else {
            backgroundImageView.startShimmeringAnimation()
            titleLabel.startShimmeringAnimation()
            timeLabel.startShimmeringAnimation()
            likeView.startShimmeringAnimation()
            circalView.startShimmeringAnimation()
        }
    }

    func bindData(data: MovieEntity) {
        titleLabel.text = data.originalTitle
        backgroundImageView.image = nil
        Task {
            backgroundImageView.image = try await delegate?.didLoadBackdropImage(path: "\(Contants.imageUrlw342)\(data.posterPath ?? .empty)")
        }
        timeLabel.text = data.releaseDate
        circalView.progress = (data.voteAverage ?? 10.0) / 10.0
        circalView.setLabelText("\(data.voteAverage ?? 0)")
        totalLikeLabel.text = "\(data.voteCount ?? 0)"
    }
}
