//
//  NowPlayingHeaderCell.swift
//  XMovie
//
//  Created by GST on 14/06/2023.
//

import UIKit

protocol NowPlayingHeaderCellDelegate: AnyObject {
    func gotoDetailMovie(movieID: Int)
    func didLoadBackdropImage(path: String) async throws -> UIImage?
}

class NowPlayingHeaderCell: UICollectionViewCell {

    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    weak var delegate: NowPlayingHeaderCellDelegate?
    let flowLayout = NormalFlowLayout()
    var dataNowPlaying: [MovieEntity] = [MovieEntity]()
    var didGetData: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        didGetData = false
        setupCollectionView()
    }
    
    func bindData(data: [MovieEntity]) {
        dataNowPlaying = data
        didGetData = true
        mainCollectionView.reloadData()
    }

    private func setupCollectionView() {
        mainCollectionView.registerCell(NowPlayingCollectionCell.self)
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        mainCollectionView.collectionViewLayout = flowLayout
        mainCollectionView.contentInsetAdjustmentBehavior = .always
    }
}

// MARK: - UITableViewDelegate
extension NowPlayingHeaderCell: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if didGetData {
            delegate?.gotoDetailMovie(movieID: dataNowPlaying[indexPath.row].id ?? 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
}

// MARK: - UITableViewDelegate
extension NowPlayingHeaderCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if didGetData {
            return dataNowPlaying.count
        } else {
            return 10
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NowPlayingCollectionCell.className, for: indexPath) as? NowPlayingCollectionCell else {return UICollectionViewCell() }
        if didGetData {
            cell.configShimmerCell(isShimmer: false)
            cell.bindData(data: dataNowPlaying[indexPath.row])
            cell.delegate = self
        } else {
            cell.configShimmerCell(isShimmer: true)
        }
        return cell
    }
}

extension NowPlayingHeaderCell: NowPlayingCollectionCellDelegate {
    func didLoadBackdropImage(path: String) async throws -> UIImage? {
        try await delegate?.didLoadBackdropImage(path: path)
    }
}
