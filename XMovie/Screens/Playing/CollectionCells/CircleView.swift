//
//  CircleView.swift
//  XMovie
//
//  Created by GST on 23/06/2023.
//

import UIKit

class CircleView: UIView {
    private let progressLayer = CAShapeLayer()
    private let gradientLayer = CAGradientLayer()
    private let label = UILabel()
    
    var progress: CGFloat = 0 {
        didSet {
            updateProgress()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupBackgroundLayer()
        setupProgressLayer()
        setupGradientLayer()
        setupLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupBackgroundLayer()
        setupProgressLayer()
        setupGradientLayer()
        setupLabel()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        progressLayer.path = createCirclePath().cgPath
        gradientLayer.frame = bounds
        gradientLayer.mask = progressLayer
        label.frame = bounds
    }
    
    private func setupProgressLayer() {
        progressLayer.strokeColor = UIColor.black.cgColor
        progressLayer.fillColor = UIColor.clear.cgColor
        progressLayer.lineWidth = 3
        progressLayer.lineCap = .round

        layer.addSublayer(progressLayer)
    }
    
    private func setupGradientLayer() {
        let gradientColors: [CGColor] = [UIColor.green.cgColor, UIColor.green.cgColor]
        gradientLayer.colors = gradientColors
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        
        layer.addSublayer(gradientLayer)
    }
    
    private func setupBackgroundLayer() {
        let grayCircleLayer = CAShapeLayer()
        grayCircleLayer.strokeColor = UIColor.gray.cgColor
        grayCircleLayer.fillColor = UIColor.clear.cgColor
        grayCircleLayer.lineWidth = 3
        grayCircleLayer.lineCap = .round
        grayCircleLayer.path = createCirclePath().cgPath

        // Add the gray circle layer to the view's layer
        layer.addSublayer(grayCircleLayer)
     }
    
    private func setupLabel() {
        label.textAlignment = .center
        label.textColor = .white
        label.font = AppConstants.FontQuickSans.medium.fontSize(12)
        label.adjustsFontSizeToFitWidth = true
        addSubview(label)
    }
    
    private func updateProgress() {
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = progressLayer.strokeEnd
        animation.toValue = progress
        animation.duration = 0.5
        progressLayer.add(animation, forKey: "strokeAnimation")
        progressLayer.strokeEnd = progress
    }
    
    private func createCirclePath() -> UIBezierPath {
        let center = CGPoint(x: bounds.midX, y: bounds.midY)
        let radius = min(bounds.width, bounds.height) / 2 - progressLayer.lineWidth / 2
        return UIBezierPath(arcCenter: center, radius: radius, startAngle: -.pi / 2, endAngle: 3 * .pi / 2, clockwise: true)
    }
    
    func setLabelText(_ text: String) {
        label.text = text
    }
}
