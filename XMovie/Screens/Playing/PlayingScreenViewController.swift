//  
//  PlayingScreenViewController.swift
//  XMovie
//
//  Created by GST on 08/06/2023.
//

import UIKit

final class PlayingScreenViewController: BaseViewController {
    
    var section: [PlayingScreenSection] = []
    
    @IBOutlet weak var collectionviewIndicator: UIActivityIndicatorView!
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    // MARK: - Variables
    var presenter = PlayingScreenPresenter(model: PlayingScreenModel())
    var refreshControl = UIRefreshControl()
    
    // MARK: - IBOutlet
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionviewIndicator.isHidden = true
        presenter.attach(view: self)
        setupCollectionView()
        Task {
           try await presenter.requestData()
            didGetData = true
            mainCollectionView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func setupCollectionView() {
        mainCollectionView.registerCell(ExploreCollectionCell.self)
        mainCollectionView.registerCell(NowPlayingHeaderCell.self)
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        mainCollectionView.backgroundColor = .black
        let flowLayout = UICollectionViewFlowLayout()
        mainCollectionView.collectionViewLayout = flowLayout
        refreshControl.addTarget(self, action: #selector(didPullToRefresh(_:)), for: .valueChanged)
        mainCollectionView.refreshControl = refreshControl
    }
    
    func navitoDetailScreen(movieID: Int) {
        let detailVC: DetailMovieViewController = DetailMovieViewController.initFromStoryboard(feature: .detail)
        detailVC.movieID = movieID
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    @objc func didPullToRefresh(_ sender: Any) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            Task {
                try await self.presenter.requestData(isRefresh: true)
                self.refreshControl.endRefreshing()
                self.mainCollectionView.reloadData()
            }
        }
    }
}

extension PlayingScreenViewController: PlayingScreenViewProtocol {
    func displayIndicator(isDisplay: Bool) {
        DispatchQueue.main.async {
            self.displayActivityIndicatorView(display: isDisplay)
        }
    }
    
    func reloadCollectionView() {
        DispatchQueue.main.async {
            self.mainCollectionView.reloadData()
        }
    }
    
    func setdidGetData() {
        didGetData = true
    }
    
    func needSetBottomIndicator(isShow: Bool) {
        DispatchQueue.main.async {
            self.collectionviewIndicator.isHidden = !isShow
            if isShow {
                self.collectionviewIndicator.startAnimating()
            } else {
                self.collectionviewIndicator.stopAnimating()
            }
        }
    }
}

// MARK: - UITableViewDelegate
extension PlayingScreenViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if didGetData {
            return presenter.numberOfRowsInSection(section: section + 1)
        } else {
            return 10
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NowPlayingHeaderCell.className, for: indexPath) as? NowPlayingHeaderCell else {return UICollectionViewCell() }
            if didGetData {
                cell.didGetData = true
                cell.bindData(data: presenter.nowPlaying)
                cell.delegate = self
            } else {
                cell.didGetData = false
            }
            return cell
        } else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ExploreCollectionCell.className, for: indexPath) as? ExploreCollectionCell else {return UICollectionViewCell() }
            if didGetData {
                cell.bindData(data: presenter.cellForExploreRowAt(indexPath: indexPath))
                cell.delegate = self
            } else {
                
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if !didGetData {
            isStartAnimation(isStart: true)
        }
    }
}

extension PlayingScreenViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if didGetData {
            if indexPath.row == 0 {
                return
            } else {
                navitoDetailScreen(movieID: presenter.cellForExploreRowAt(indexPath: indexPath).id ?? 0)
            }
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension PlayingScreenViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        // margin left, center and right
        if indexPath.row != 0 {
            return CGSize(width: (collectionView.frame.width - AppConstants.margin * 2.0) / 2.0, height: 290)
        } else {
            return CGSize(width: collectionView.frame.width, height: 280)
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: AppConstants.margin / 2, bottom: AppConstants.margin, right: AppConstants.margin / 2)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        AppConstants.margin
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        AppConstants.margin
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
        .zero
    }
}

extension PlayingScreenViewController: NowPlayingHeaderCellDelegate {
    func gotoDetailMovie(movieID: Int) {
        self.navitoDetailScreen(movieID: movieID)
    }
}

extension PlayingScreenViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.panGestureRecognizer.translation(in: scrollView).y < 0 {
            animateTabBar(hidden: true)
        } else {
            animateTabBar(hidden: false)
        }
    }
}

extension PlayingScreenViewController: ExploreCollectionCellDelegate {
    func didLoadBackdropImage(path: String) async throws -> UIImage? {
        try await presenter.dowloadImage(path: path)
    }
}
