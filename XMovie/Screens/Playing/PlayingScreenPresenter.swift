//  
//  PlayingScreenPresenter.swift
//  XMovie
//
//  Created by GST on 08/06/2023.
//

import Foundation
import UIKit

final class PlayingScreenPresenter {
    weak var contentView: PlayingScreenContract.View?
    private var model: PlayingScreenContract.Model
    var nowPlaying: [MovieEntity] = [MovieEntity]()
    var exploreMovie: [MovieEntity] = [MovieEntity]()
    var pageExplore: Int = 1

    init(model: PlayingScreenContract.Model) {
        self.model = model
    }
}

extension PlayingScreenPresenter: PlayingScreenPresenterProtocol {
    func attach(view: PlayingScreenContract.View) {
        contentView = view
    }
    
    func detachView() {
        contentView = nil
    }
    
    func requestData(isRefresh: Bool = false) async throws {
        if isRefresh {
            pageExplore = 1
            nowPlaying.removeAll()
            exploreMovie.removeAll()
        }
        do {
            contentView?.displayIndicator(isDisplay: true)
            nowPlaying.append(contentsOf: try await model.getNowPlayingMovie(page: 1).results)
            exploreMovie.append(contentsOf: try await model.getExploreMovie(page: pageExplore).results)
            contentView?.displayIndicator(isDisplay: false)
        } catch {
            print("GetDataError")
            contentView?.displayIndicator(isDisplay: false)
        }
    }
    
    func getExploreMovie() async throws {
        do {
            exploreMovie.append(contentsOf: try await model.getExploreMovie(page: pageExplore).results)
        } catch {
            print("GetDataError")
            contentView?.displayIndicator(isDisplay: false)
        }
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return exploreMovie.count
    }

    func cellForExploreRowAt(indexPath: IndexPath) -> MovieEntity {
        // Check if the last row number is the same as the last current data element
            if indexPath.row == self.exploreMovie.count - 1 {
                self.pageExplore += 1
                contentView?.needSetBottomIndicator(isShow: true)
                Task {
                    try await self.getExploreMovie()
                    contentView?.needSetBottomIndicator(isShow: false)
                    contentView?.reloadCollectionView()
                }
            }
        return exploreMovie.count == 0 ? MovieEntity() : exploreMovie[indexPath.row]
    }
    
    func dowloadImage(path: String) async throws -> UIImage {
        do {
            return try await model.dowloadImage(path: path)
        } catch {
            return UIImage()
        }
    }
}
