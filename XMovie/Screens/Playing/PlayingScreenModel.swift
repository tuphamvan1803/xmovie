//  
//  PlayingScreenModel.swift
//  XMovie
//
//  Created by GST on 08/06/2023.
//

import Foundation
import UIKit

enum PlayingScreenSection {
    case nowPlaying
    case explore
}

struct PlayingScreenViewEntity {
    
}

final class PlayingScreenModel {
    private var homeService: HomeServices = HomeServices()
}

extension PlayingScreenModel: PlayingScreenModelProtocol {
    func getNowPlayingMovie(page: Int) async throws -> ListMovies {
        try await withCheckedThrowingContinuation({ continuation in
            homeService.getListHomeMovies(page: page, type: Contants.nowPlayingType, completion: { success, results, error in
                if success, let results {
                    continuation.resume(returning: results)
                } else {
                    continuation.resume(throwing: error)
                }
            })
        })
    }
    
    func getExploreMovie(page: Int) async throws -> ListMovies  {
        try await withCheckedThrowingContinuation({ continuation in
            homeService.getListHomeMovies(page: page, type: Contants.upcommingType, completion: { success, results, error in
                if success, let results {
                    continuation.resume(returning: results)
                } else {
                    continuation.resume(throwing: error)
                }
            })
        })
    }
    
    func dowloadImage(path: String) async throws -> UIImage {
        try await withCheckedThrowingContinuation({ continuation in
            homeService.loadPhoto(path) { image, error in
                if let error = error {
                    continuation.resume(throwing: error)
                }
                if let image = image {
                    continuation.resume(returning: image)
                }
            }
        })
    }
}
