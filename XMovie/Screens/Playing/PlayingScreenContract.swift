//  
//  PlayingScreenContract.swift
//  XMovie
//
//  Created by GST on 08/06/2023.
//

import Foundation
import UIKit

protocol PlayingScreenContract {
    typealias Model = PlayingScreenModelProtocol
    typealias View = PlayingScreenViewProtocol
    typealias Presenter = PlayingScreenPresenterProtocol
}

protocol PlayingScreenModelProtocol {
    func getNowPlayingMovie(page: Int) async throws -> ListMovies
    func getExploreMovie(page: Int) async throws -> ListMovies
    func dowloadImage(path: String) async throws -> UIImage
}

protocol PlayingScreenViewProtocol: AnyObject {
    func displayIndicator(isDisplay: Bool)
    func reloadCollectionView()
    func setdidGetData()
    func needSetBottomIndicator(isShow: Bool)
}

protocol PlayingScreenPresenterProtocol {
    func attach(view: PlayingScreenContract.View)
    func detachView()
}
