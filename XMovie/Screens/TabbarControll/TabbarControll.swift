//
//  TabbarControll.swift
//  XMovie
//
//  Created by GST on 08/06/2023.
//

import UIKit

@available(iOS 13.0, *)
final class TabbarControll: UITabBarController {
    
    //MARK: - Properties
    private lazy var homeViewController: HomeScreenViewController = {
        let homeViewController: HomeScreenViewController = HomeScreenViewController.initFromStoryboard(feature: .home)
        homeViewController.tabBarItem.image = UIImage(named: "home")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        homeViewController.tabBarItem.selectedImage = UIImage(named: "homeSellected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        homeViewController.tabBarItem.title = "Discover"
        return homeViewController
    }()
    
    private lazy var playingViewController: PlayingScreenViewController = {
        let playingViewController: PlayingScreenViewController = PlayingScreenViewController.initFromStoryboard(feature: .playing)
        playingViewController.tabBarItem.image = UIImage(named: "playing")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        playingViewController.tabBarItem.title = "Playing"
        playingViewController.tabBarItem.selectedImage = UIImage(named: "playingSellected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        return playingViewController
    }()
    
    private lazy var profileViewController: ProfileScreenViewController = {
        let profileViewController: ProfileScreenViewController = ProfileScreenViewController.initFromStoryboard(feature: .profile)
        profileViewController.tabBarItem.image = UIImage(named: "profile")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        profileViewController.tabBarItem.title = "Profile"
        profileViewController.tabBarItem.selectedImage = UIImage(named: "profileSellected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        return profileViewController
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        customTabbarTitle()
        
        let homeNavigation = UINavigationController.init(rootViewController: homeViewController)
        let playingNavigation = UINavigationController.init(rootViewController: playingViewController)
        let profileNavigation = UINavigationController.init(rootViewController: profileViewController)
        
        self.viewControllers = [homeNavigation, playingNavigation , profileNavigation]
        tabBar.backgroundColor = ColorDefine.color1E1E1E
        tabBar.tintColor = ColorDefine.colorAAA6A6
        tabBar.layer.borderColor = ColorDefine.colorFD5151.cgColor
        tabBar.layer.borderWidth = 1
        tabBar.layer.cornerRadius = tabBar.bounds.height / 2
        selectedIndex = 0
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let tabBarHeight: CGFloat = 64
        let tabBarPadding: CGFloat = 45
        let tabBarWidth = view.bounds.width - (2 * tabBarPadding)
        let tabBarX = tabBarPadding
        let tabBarY = view.bounds.height - tabBarHeight - 30
        tabBar.frame = CGRect(x: tabBarX, y: tabBarY, width: tabBarWidth, height: tabBarHeight)
        tabBar.layer.cornerRadius = tabBar.bounds.height / 2
    }
    
    private func customTabbarTitle() {
        let appearance = UITabBarAppearance()
        appearance.stackedLayoutAppearance.selected.titleTextAttributes = [NSAttributedString.Key.font: AppConstants.FontQuickSans.demiBold.fontSize(14),
                                                                           NSAttributedString.Key.foregroundColor: ColorDefine.colorFD5151]
        appearance.stackedLayoutAppearance.normal.titleTextAttributes = [NSAttributedString.Key.font: AppConstants.FontQuickSans.demiBold.fontSize(12),
                                                                         NSAttributedString.Key.foregroundColor: UIColor.darkGray]
        tabBar.standardAppearance = appearance
        self.tabBarItem.imageInsets = UIEdgeInsets(top: 3, left: 3, bottom: 1, right: 3)
    }
}
