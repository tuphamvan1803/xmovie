//
//  DetailMainContentCell.swift
//  XMovie
//
//  Created by GST on 24/06/2023.
//

import UIKit

protocol DetailMainContentCellDelegate: AnyObject {
    func didLoadBackdropImage(path: String) async throws -> UIImage?
}

class DetailMainContentCell: UITableViewCell {

    @IBOutlet private weak var contentImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet private weak var voteAvengerLabel: UILabel!
    @IBOutlet private weak var yearPublishLabel: UILabel!
    @IBOutlet private weak var totalTimeLabel: UILabel!
    @IBOutlet private weak var categoryStackView: UIStackView!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var castLabel: UILabel!
    @IBOutlet private weak var directorLabel: UILabel!
    @IBOutlet private weak var rateView: UIView!
    @IBOutlet private weak var playView: UIView!
    @IBOutlet private weak var opacityView: UIView!
    
    weak var delegate: DetailMainContentCellDelegate?
    var dataCast: String = .empty
    var dataDetail: ListMovieDetailEntity?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        rateView.layer.cornerRadius = 8
        playView.layer.cornerRadius = 8
        addGradientToView(view: opacityView)
    }
    
    func bindData(data: ListMovieDetailEntity, credit: ListCategoryResponseEntity) {
        self.dataDetail = data
        titleLabel.text = data.originalTitle
        contentImageView.image = nil
        Task {
            contentImageView.image = try await delegate?.didLoadBackdropImage(path: "\(Contants.imageUrl)\(data.posterPath)")
        }
        voteAvengerLabel.text = "\(data.voteAverage )"
        yearPublishLabel.text = data.releaseDate.toStringVnWith(fromDateFormat: Contants.dateFormatyyyyMMdd,
                                                                toDateFormat: Contants.yearFormatyyyy)
        descriptionLabel.text = data.overview
        totalTimeLabel.text = secondsToHoursMinutesSeconds(seconds: data.runtime)
        setupCastLabel(data: credit.cast)
        setupDirectorLabel(data: credit.crew)
        setupCategoryLabel(data: data)
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> String {
        let time = "\((seconds % 3600) / 60)h \((seconds % 3600) % 60)m"
      return time
    }
        
    func setupCategoryLabel(data: ListMovieDetailEntity) {
       removeAllSubview()
        for item in data.genres {
            let view = DetailCategoryView()
            view.loadContentViewWithNib(nibName: String(describing: DetailCategoryView.self))
            view.setupData(category: item.name)
            self.categoryStackView.addArrangedSubview(view)
        }
    }
    
    func removeAllSubview() {
        self.categoryStackView.subviews.forEach { subview in
            self.categoryStackView.removeArrangedSubview(subview)
            subview.removeFromSuperview()
        }
    }
    
    func setupDirectorLabel(data: [ListCastResponseEntitty]) {
        for item in data where item.job == "Director" {
            directorLabel.text = "Director: \(item.name)"
        }
    }
    
    func setupCastLabel(data: [ListCastResponseEntitty]) {
        let dataCast = data.map { $0.name }
        castLabel.text = "Cast: \(dataCast.joined(separator: ", "))"
        self.dataCast =  "Cast: \(dataCast.joined(separator: ", "))"
        castLabel.numberOfLines = 1
        castLabel.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(labelTapped))
        castLabel.addGestureRecognizer(tapGesture)
        if shouldAddMoreText(to: castLabel) {
            addMoreText(to: castLabel)
        }
    }
    
    @objc func labelTapped() {
        showFullLabel()
    }
    
    func showFullLabel() {
        castLabel.numberOfLines = 0
        castLabel.text = dataCast
        layoutIfNeeded()
    }
}
