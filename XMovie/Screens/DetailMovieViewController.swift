//  
//  DetailMovieViewController.swift
//  XMovie
//
//  Created by GST on 24/06/2023.
//

import UIKit

final class DetailMovieViewController: BaseViewController {
    // MARK: - Variables
    var presenter = DetailMoviePresenter(model: DetailMovieModel())
    var section: [DetailMovieSection] = [DetailMovieSection]()
    var movieID: Int = 0
    var reloadDetailMovieCalled: Bool = false
    
    // MARK: - IBOutlet
    @IBOutlet weak var bookingNowButton: UIButton!
    @IBOutlet weak var mainTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attach(view: self)
        setupUI()
        setupTableView()
        Task {
            try await presenter.requestData(movieID: self.movieID)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        animateTabBar(hidden: true)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        animateTabBar(hidden: false)
    }
    
    func setupUI() {
        section = [.header, .more]
        bookingNowButton.layer.cornerRadius = bookingNowButton.bounds.height / 2 - 2
    }
    
    func setupTableView() {
        mainTableView.registerCell(DetailMainContentCell.self)
        mainTableView.registerCell(DetailMoreLikeThisCell.self)
        mainTableView.register(HomeHeaderSection.self, forHeaderFooterViewReuseIdentifier: HomeHeaderSection.className)
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.separatorStyle = .none
        mainTableView.contentInset = UIEdgeInsets(top: -60, left: 0, bottom: 0, right: 0)
        mainTableView.backgroundColor = .black
    }
    
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension DetailMovieViewController: DetailMovieViewProtocol {
    func displayIndicator(isDisplay: Bool) {
        DispatchQueue.main.async {
            self.displayActivityIndicatorView(display: isDisplay)
        }
    }
    
    func reloadCollectionView() {
        DispatchQueue.main.async {
            self.didGetData = true
            self.mainTableView.reloadData()
        }
    }
}

// MARK: - UITableViewDelegate
extension DetailMovieViewController: UITableViewDelegate {
    func setdidGetData() {
        didGetData = true
    }
}

// MARK: - UITableViewDataSource
extension DetailMovieViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.section.count
    }
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.section[section] {
        case .header:
            return 1
        case .more:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.section[indexPath.section] {
        case .header:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailMainContentCell.className, for: indexPath) as? DetailMainContentCell else {
                return UITableViewCell()
            }
            if didGetData {
                if let dataCell = presenter.detailMovie, let dataCredit = presenter.creditMovie {
                    cell.bindData(data: dataCell,
                                  credit: dataCredit)
                    cell.delegate = self
                }
            } else {
            }
            return cell
        case .more:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailMoreLikeThisCell.className, for: indexPath) as? DetailMoreLikeThisCell else {
                return UITableViewCell()
            }
            if didGetData {
                cell.didGetData = true
                cell.bindData(data: presenter.similarMovie)
                cell.delegate = self
            } else {
                cell.didGetData = false
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !didGetData {
            isStartAnimation(isStart: true)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch self.section[section] {
        case .header:
            return UIView()
        case .more:
            guard let headerViewCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: HomeHeaderSection.className) as? HomeHeaderSection else {
                return UIView()
            }
            headerViewCell.setupTitle(title: "More like this")
            return headerViewCell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch self.section[section] {
        case .header:
            return .leastNonzeroMagnitude
        case .more:
            return 35
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch self.section[indexPath.section] {
        case .header:
            return mainTableView.bounds.height * 2 / 3
        case .more:
            return UITableView.automaticDimension
        }
    }
}

extension DetailMovieViewController: DetailMoreLikeThisCellDelegate {
    func reloadDetailMovie(movieID: Int) {
        Task.detached { [weak self] in
            guard let self = self else { return }
            try await self.presenter.requestData(movieID: movieID)
            DispatchQueue.main.async {
                self.mainTableView.reloadData()
            }
        }
        reloadDetailMovieCalled = true
    }
}

extension DetailMovieViewController: DetailMainContentCellDelegate {
    func didLoadBackdropImage(path: String) async throws -> UIImage? {
        try await presenter.dowloadImage(path: path)
    }
}
