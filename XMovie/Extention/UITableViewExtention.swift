//
//  UITableViewExtention.swift
//  XMovie
//
//  Created by GST on 24/06/2023.
//

import Foundation
import UIKit

public extension UITableView {
    func registerCell<T: UITableViewCell>(_ type: T.Type) {
        let className = String(describing: T.self)
        self.register(UINib(nibName: className, bundle: nil), forCellReuseIdentifier: className)
        
    }
}
public extension UICollectionView {
    func registerCell<T: UICollectionViewCell>(_ type: T.Type) {
        let className = String(describing: T.self)
        self.register(UINib(nibName: className, bundle: nil), forCellWithReuseIdentifier: className)
    }
}

public extension UITableView {
    func registerView<T: UITableViewHeaderFooterView>(_ type: T.Type) {
        let className = String(describing: T.self)
        self.register(UINib(nibName: className, bundle: nil), forHeaderFooterViewReuseIdentifier: className)
    }
}

public extension NSObject {
    class var className: String {
        String(describing: self)
    }
}

public extension UIViewController {
    class func loadFromNib<T: UIViewController>(type: T.Type) -> T {
        return T(nibName: String(describing: T.self), bundle: nil)
    }
    
    class func initFromStoryboard<T: UIViewController>(feature: AppScreen, identify: String? = nil) -> T {
        let viewControllerIdentify = identify ?? String(describing: T.self)
        return UIStoryboard.init(name: feature.storyBoardName, bundle: nil)
            .instantiateViewController(withIdentifier: viewControllerIdentify) as! T
    }
}
